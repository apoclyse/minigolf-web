export {};

declare global {
  interface Array<T> {
    distinct<T>(): Array<T>;
    distinctByKey<T, D>(keyFunction: (item: T) => D): T[];
    diff<T, D>(keyFunction: (item: T) => D, array: T[]): T[];
  }
}

Array.prototype.distinct = function <D>(): Array<D> {
  if (this.length === 0) {
    return this;
  }
  return this.filter((x, i, a) => a.indexOf(x) === i);
};

Array.prototype.distinctByKey = function <T, D>(
  keyFunction: (item: T) => D
): T[] {
  if (this.length === 0) {
    return this;
  }
  return this.filter((x, i, a) => {
    return a.findIndex((item) => keyFunction(item) === keyFunction(x)) === i;
  });
};

Array.prototype.diff = function <T, D>(
  keyFunction: (item: T) => D,
  array: T[]
): T[] {
  const difference: T[] = [];
  this.forEach((el: T) => {
    const elFound = array?.find((v) => keyFunction(v) === keyFunction(el));
    if (!elFound) {
      difference.push(el);
    }
  });
  return difference;
};
