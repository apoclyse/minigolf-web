import { Pipe, PipeTransform } from '@angular/core';
import { ClubsService } from '../pages/clubs/services/clubs.service';
import { ClubDto } from '../models/club-dto';
import { Observable } from 'rxjs';

@Pipe({
  name: 'clubById',
})
export class ClubByIdPipe implements PipeTransform {
  constructor(private clubsService: ClubsService) {}

  transform(id: number): Observable<ClubDto> {
    return this.clubsService.getCached(id);
  }
}
