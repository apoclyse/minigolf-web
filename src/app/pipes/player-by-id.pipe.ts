import { Pipe, PipeTransform } from '@angular/core';
import { PlayerDto } from '../models/player-dto';
import { PlayersService } from '../pages/players/services/players.service';
import {Observable, of} from 'rxjs';

@Pipe({
  name: 'playerById',
})
export class PlayerByIdPipe implements PipeTransform {
  constructor(private playersService: PlayersService) {}

  transform(id: number): Observable<PlayerDto> {
    return id ? this.playersService.getCached(id) : of(null);
  }
}
