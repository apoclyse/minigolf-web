import { Pipe, PipeTransform } from '@angular/core';
import { EventDto } from '../models/event-dto';

@Pipe({
  name: 'eventTag',
})
export class EventTagPipe implements PipeTransform {
  LIVE = 'LIVE';
  AVAILABLE = 'AVAILABLE';

  transform(eventDto: EventDto): string {
    const currDate = new Date();
    currDate.setHours(0, 0, 0, 0);

    const dateFromTime = eventDto.dateFrom.getTime();
    const currDateTime = currDate.getTime();
    if (!eventDto.dateTo) {
      return currDateTime == dateFromTime ? this.LIVE : this.AVAILABLE;
    }
    return currDateTime >= dateFromTime &&
      currDateTime <= eventDto.dateTo.getTime()
      ? this.LIVE
      : this.AVAILABLE;
  }
}
