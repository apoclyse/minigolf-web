import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'scoreSum',
})
export class ScoreSumPipe implements PipeTransform {
  transform(o: object): number {
    return Object.values(o).reduce((sum, v) => sum + v ?? 0, 0) || null;
  }
}
