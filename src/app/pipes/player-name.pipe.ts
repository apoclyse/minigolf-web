import {Pipe, PipeTransform} from '@angular/core';
import {PlayerDto} from '../models/player-dto';

@Pipe({
  name: 'playerName',
})
export class PlayerNamePipe implements PipeTransform {
  transform(playerDto: PlayerDto): string {
    return `${playerDto.firstname} ${playerDto.lastname} - ${playerDto.category}`;
  }
}
