import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roundColor',
})
export class RoundColorPipe implements PipeTransform {
  transform(value: number): string {
    if (value < 20) {
      return 'blue';
    } else if (value < 25) {
      return 'green';
    } else if (value < 30) {
      return 'red';
    }
    return 'black';
  }
}
