import { Pipe, PipeTransform } from '@angular/core';
import { Score } from '../models/score';
import { ResultCategory } from '../models/result-category';
import { PlayerCategory } from '../models/player-category';

@Pipe({
  name: 'scoreByCategory',
})
export class ScoreByCategoryPipe implements PipeTransform {
  transform(scores: Score[], category: ResultCategory): Score[] {
    const playerCategory = category as unknown as PlayerCategory;
    return category !== ResultCategory.ALL
      ? scores?.filter(
          (score) =>
            score.category ? score.category === playerCategory : score.player.category === playerCategory
        )
      : scores;
  }
}
