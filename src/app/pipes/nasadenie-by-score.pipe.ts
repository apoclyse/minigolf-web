import { Pipe, PipeTransform } from '@angular/core';
import { ScoreDto } from '../models/score-dto';
import { TeamDto } from '../models/team-dto';
import { nasadenie } from '../constants/nasadenie';

@Pipe({
  name: 'nasadenieByScore',
})
export class NasadenieByScorePipe implements PipeTransform {
  transform(teamDtos: TeamDto[], score: ScoreDto): number {
    const foundIndex = teamDtos.findIndex(
      (team) => team.clubId === score.clubId
    );
    if (foundIndex === -1) {
      return null;
    }
    return nasadenie.find(
      (n) => teamDtos[foundIndex]['player' + n] === score.playerId
    );
  }
}
