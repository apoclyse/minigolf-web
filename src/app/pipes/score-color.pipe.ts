import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'scoreColor',
})
export class ScoreColorPipe implements PipeTransform {
  transform(value: number): string {
    if (value === 1) {
      return 'blue';
    } else if (value === 2) {
      return 'green';
    } else if (value === 3) {
      return 'red';
    }
    return 'black';
  }
}
