import {Pipe, PipeTransform} from '@angular/core';
import {MenuItem} from 'primeng-lts/api';
import {LoginDto} from "../pages/login/model/login-dto";

@Pipe({
  name: 'menuFilter',
})
export class MenuFilterPipe implements PipeTransform {
  transform(login: LoginDto, menuItems: MenuItem[]): MenuItem[] {
    return login
      ? menuItems
      : menuItems.filter((item) => !item.state?.secureRead);
  }
}
