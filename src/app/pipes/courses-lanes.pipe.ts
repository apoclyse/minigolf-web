import { Pipe, PipeTransform } from '@angular/core';
import { CourseDto } from '../models/course-dto';

@Pipe({
  name: 'coursesLanes',
})
export class CoursesLanesPipe implements PipeTransform {
  transform(courses: CourseDto[], laneNumber: number): string {
    return courses?.reduce(
      (label, course, idx) =>
        label +
        `${course['lane' + laneNumber]} ${
          idx < courses.length - 1 ? '/ ' : ''
        }`,
      ''
    );
  }
}
