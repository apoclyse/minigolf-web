import { Pipe, PipeTransform } from '@angular/core';
import {ClubDto} from "../models/club-dto";

@Pipe({
  name: 'clubByIdFromService'
})
export class ClubByIdFromServicePipe implements PipeTransform {

  transform(clubs: ClubDto[], clubId: number): ClubDto {
    return clubs?.find((club) => club.id === clubId);
  }

}
