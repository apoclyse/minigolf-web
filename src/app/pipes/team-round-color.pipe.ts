import { Pipe, PipeTransform } from '@angular/core';
import { TeamRound } from '../models/team-round';

@Pipe({
  name: 'teamRoundColor',
})
export class TeamRoundColorPipe implements PipeTransform {
  transform(teamRound: TeamRound): string {
    if (!teamRound) return '';
    const value = teamRound.sum / teamRound.roundsCount;
    if (value < 20) {
      return 'blue';
    } else if (value < 25) {
      return 'green';
    } else if (value < 30) {
      return 'red';
    }
    return 'black';
  }
}
