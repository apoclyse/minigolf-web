import { Pipe, PipeTransform } from '@angular/core';
import { PlayerDto } from '../models/player-dto';

@Pipe({
  name: 'playerFind',
})
export class PlayerFindPipe implements PipeTransform {
  transform(id: number, playerDtos: PlayerDto[]): PlayerDto {
    return playerDtos?.find((player) => player.id === id) ?? null;
  }
}
