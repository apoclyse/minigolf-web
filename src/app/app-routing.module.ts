import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SecureRoutingService } from './services/secure-routing.service';

const routes: Routes = [
  {
    path: 'clubs',
    canActivate: [SecureRoutingService],
    data: { secureRead: true },
    loadChildren: () =>
      import('./pages/clubs/clubs.module').then((m) => m.ClubsModule),
  },
  {
    path: 'courses',
    canActivate: [SecureRoutingService],
    data: { secureRead: true },
    loadChildren: () =>
      import('./pages/courses/courses.module').then((m) => m.CoursesModule),
  },
  {
    path: 'events',
    canActivate: [SecureRoutingService],
    data: { secureRead: true },
    loadChildren: () =>
      import('./pages/events/events.module').then((m) => m.EventsModule),
  },
  {
    path: 'event-edit',
    canActivate: [SecureRoutingService],
    data: { secureRead: true },
    loadChildren: () =>
      import('./pages/event-edit/event-edit.module').then(
        (m) => m.EventEditModule
      ),
  },
  {
    path: 'event-write',
    canActivate: [SecureRoutingService],
    data: { secureRead: true },
    loadChildren: () =>
      import('./pages/event-write/event-write.module').then(
        (m) => m.EventWriteModule
      ),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'players',
    canActivate: [SecureRoutingService],
    data: { secureRead: true },
    loadChildren: () =>
      import('./pages/players/players.module').then((m) => m.PlayersModule),
  },
  {
    path: 'cups',
    canActivate: [SecureRoutingService],
    data: { secureRead: true },
    loadChildren: () =>
      import('./pages/cups/cups.module').then((m) => m.CupsModule),
  },
  {
    path: 'results',
    loadChildren: () =>
      import('./pages/results/results.module').then((m) => m.ResultsModule),
  },
  {
    path: 'result',
    loadChildren: () =>
      import('./pages/result/result.module').then((m) => m.ResultModule),
  },
  {
    path: 'result-player',
    loadChildren: () =>
      import('./pages/result-player/result-player.module').then(
        (m) => m.ResultPlayerModule
      ),
  },
  {
    path: 'result-write',
    canActivate: [SecureRoutingService],
    data: { secureRead: true },
    loadChildren: () =>
      import('./pages/result-write/result-write.module').then(
        (m) => m.ResultWriteModule
      ),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginModule),
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '**',
    loadChildren: () =>
      import('./pages/http-forbidden/http-forbidden.module').then(
        (m) => m.HttpForbiddenModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
