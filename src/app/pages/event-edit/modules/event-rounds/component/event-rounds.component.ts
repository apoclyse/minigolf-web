import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { CourseService } from '../../../../courses/services/course.service';
import { take } from 'rxjs/operators';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RoundsService } from '../../../../../services/rounds.service';
import { RoundUpdate } from '../../../../../models/round-update';
import { RoundCreate } from '../../../../../models/round-create';
import { MessageService } from '../../../../../services/message.service';
import { EventDto } from '../../../../../models/event-dto';
import { RoundDto } from '../../../../../models/round-dto';
import { Round } from '../../../../../models/round';

@Component({
  selector: 'app-event-rounds',
  templateUrl: './event-rounds.component.html',
  styleUrls: ['./event-rounds.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventRoundsComponent implements OnInit {
  formGroup: FormGroup = this.fb.group({
    id: null,
    eventId: [null, Validators.required],
    rounds: this.fb.array([]),
  });

  roundsLength: number;

  @Input() eventDto: EventDto;

  get roundsArray(): FormArray {
    return this.formGroup.get('rounds') as FormArray;
  }

  constructor(
    public roundsService: RoundsService,
    public courseService: CourseService,
    private fb: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.roundsService.get(this.eventDto.id).subscribe((roundDto) => {
      this.patchFormGroup(roundDto);
      this.changeDetectorRef.detectChanges();
    });
  }

  private patchFormGroup(roundDto: RoundDto): void {
    this.roundsArray.clear();
    this.formGroup.patchValue({ id: roundDto.id, eventId: roundDto.eventId });
    roundDto.rounds.forEach((round) =>
      this.roundsArray.push(
        this.fb.group({
          courseId: [round.courseId, Validators.required],
          name: [round.name, Validators.required],
          includeInTeams: [round.includeInTeam ?? true]
        })
      )
    );
    this.setRoundsLength();
    if (roundDto.rounds.length === 0) {
      this.addRound();
    }
  }

  private setRoundsLength(): void {
    this.roundsLength = this.roundsArray.length;
  }

  addRound() {
    this.roundsArray.push(
      this.fb.group({
        courseId: [null, Validators.required],
        name: [null, Validators.required],
        includeInTeams: [true]
      })
    );
  }

  removeRound(index: number) {
    this.roundsArray.removeAt(index);
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      if (this.formGroup.value.id) {
        this.roundsService
          .update(
            this.formGroup.value.id,
            new RoundUpdate(
              this.roundsArray.value.map(
                (round) => new Round(round.courseId, round.name, round.includeInTeams)
              )
            )
          )
          .pipe(take(1))
          .subscribe(() => {
            this.setRoundsLength();
            this.messageService.success('Kolá boli uložené.');
          });
      } else {
        this.roundsService
          .create(
            new RoundCreate(
              this.formGroup.value.eventId,
              this.roundsArray.value.map(
                (round) => new Round(round.courseId, round.name, round.includeInTeams)
              )
            )
          )
          .pipe(take(1))
          .subscribe(() => {
            this.setRoundsLength();
            this.messageService.success('Kolá boli vytvorené.');
          });
      }
    }
  }
}
