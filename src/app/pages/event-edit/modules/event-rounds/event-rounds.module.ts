import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventRoundsComponent } from './component/event-rounds.component';
import { ButtonModule } from 'primeng-lts/button';
import { InputTextModule } from 'primeng-lts/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng-lts/dropdown';
import { OverlayPanelModule } from 'primeng-lts/overlaypanel';
import { BadgeModule } from 'primeng-lts/badge';
import { TableModule } from "primeng-lts/table";
import { CheckboxModule } from "primeng-lts/checkbox";

@NgModule({
  declarations: [EventRoundsComponent],
  exports: [EventRoundsComponent],
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    ReactiveFormsModule,
    DropdownModule,
    OverlayPanelModule,
    BadgeModule,
    TableModule,
    CheckboxModule
  ],
})
export class EventRoundsModule {}
