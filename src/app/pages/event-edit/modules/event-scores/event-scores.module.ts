import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventScoresComponent } from './component/event-scores.component';
import { ButtonModule } from 'primeng-lts/button';
import { InputTextModule } from 'primeng-lts/inputtext';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng-lts/dropdown';
import { SharedPipesModule } from '../../../../pipes/shared-pipes.module';
import { TableModule } from 'primeng-lts/table';
import {EventRoundsModule} from "../event-rounds/event-rounds.module";

@NgModule({
  declarations: [EventScoresComponent],
  exports: [EventScoresComponent],
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    SharedPipesModule,
    TableModule,
    FormsModule,
    EventRoundsModule,
  ],
})
export class EventScoresModule {}
