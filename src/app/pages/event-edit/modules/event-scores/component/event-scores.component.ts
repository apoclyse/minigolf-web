import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { EventDto } from '../../../../../models/event-dto';
import { MessageService } from '../../../../../services/message.service';
import { mergeMap, take, tap } from 'rxjs/operators';
import { ClubsService } from '../../../../clubs/services/clubs.service';
import { ScoresService } from '../../../../../services/scores.service';
import { PlayersService } from '../../../../players/services/players.service';
import { ScoreDto } from '../../../../../models/score-dto';
import { ScoreCreate } from '../../../../../models/score-create';
import { ConfirmationService } from '../../../../../services/confirmation.service';
import { nasadenie } from '../../../../../constants/nasadenie';
import { TeamsService } from '../../../../../services/teams.service';
import { TeamDto } from '../../../../../models/team-dto';
import { Observable, of } from 'rxjs';
import { Resp } from '../../../../../models/resp';
import { TeamCreateUpdate } from '../../../../../models/team-create-update';
import { SnapshotService } from '../../../services/snapshot.service';
import { Empty } from '../../../../../models/empty';
import { PlayerDto } from '../../../../../models/player-dto';
import {playerCategories, PlayerCategory} from "../../../../../models/player-category";
import {ScoreCategoryUpdate} from "../../../../../models/score-category-update";

@Component({
  selector: 'app-event-scores',
  templateUrl: './event-scores.component.html',
  styleUrls: ['./event-scores.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventScoresComponent implements OnInit {
  scores: ScoreDto[] = [];
  teams: TeamDto[] = [];
  categories: PlayerCategory[] = playerCategories;

  nasadenie: number[] = nasadenie;

  @Input() eventDto: EventDto;

  constructor(
    public scoresService: ScoresService,
    public clubsService: ClubsService,
    public teamsService: TeamsService,
    public playersService: PlayersService,
    private fb: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private snapshotService: SnapshotService
  ) {}

  ngOnInit(): void {
    this.scoresService
      .getAll(this.eventDto.id)
      .pipe(
        tap((scoreDtos) => {
          this.scores = scoreDtos;
          this.refresh();
        }),
        mergeMap(() => this.teamsService.getAll(this.eventDto.id)),
        tap((teamDtos) => {
          this.teams = teamDtos;
        }),
        mergeMap(() => this.snapshotService.get(this.eventDto.id)),
        tap((snapshotDto) => {
          if (snapshotDto) {
            let ids: number[] = JSON.parse(snapshotDto.snapshot);
            if (ids.length !== this.scores.length - 1) {
              this.messageService.warn(
                'Uložené poradie hráčov nie je aktualizované.'
              );
            }
            ids = ids.filter((id) => this.scores.some((s) => s.id === id));
            const missingIds = [
              ...this.scores.filter((s) => !ids.includes(s.id)),
            ];
            this.scores = ids
              .map((id) => this.scores.find((s) => s.id === id))
              .concat(missingIds);
          }
        })
      )
      .subscribe(() => {
        this.changeDetectorRef.detectChanges();
      });
  }

  private createUpdateTeam(
    scoreDto: ScoreDto,
    nasadenie: number
  ): Observable<Resp> {
    const foundIndex = this.teams.findIndex(
      (team) => team.clubId === scoreDto.clubId
    );
    if (foundIndex !== -1) {
      const teamUpdate = TeamCreateUpdate.update(
        this.teams[foundIndex],
        scoreDto.playerId,
        nasadenie
      );
      return this.teamsService
        .update(this.teams[foundIndex].id, teamUpdate)
        .pipe(
          tap(() => {
            this.teams[foundIndex] = Object.assign(
              this.teams[foundIndex],
              teamUpdate
            );
          })
        );
    } else {
      const teamCreate = TeamCreateUpdate.create(
        this.eventDto.id,
        scoreDto.clubId,
        scoreDto.playerId,
        nasadenie
      );
      return this.teamsService.create(teamCreate).pipe(
        tap(({ insertId }) => {
          this.teams.push({ id: insertId, created: null, ...teamCreate });
        })
      );
    }
  }

  private removeFromTeam(scoreDto: ScoreDto): Observable<Empty> {
    const foundIndex = this.teams.findIndex(
      (team) => team.clubId === scoreDto.clubId
    );
    if (foundIndex === -1) return of(null);

    const teamPlayers = nasadenie
      .map((i) => this.teams[foundIndex]['player' + i] as number)
      .filter((v) => v);

    if (teamPlayers.length <= 1) {
      return this.teamsService.delete(this.teams[foundIndex].id).pipe(
        tap(() => {
          this.teams.splice(foundIndex, 1);
        })
      );
    } else if (teamPlayers.includes(scoreDto.playerId)) {
      const teamUpdate = TeamCreateUpdate.delete(
        this.teams[foundIndex],
        scoreDto.playerId
      );
      return this.teamsService
        .update(this.teams[foundIndex].id, teamUpdate)
        .pipe(
          tap(() => {
            this.teams[foundIndex] = Object.assign(
              this.teams[foundIndex],
              teamUpdate
            );
          })
        );
    }
    return of(null);
  }

  refresh(): void {
    this.scores = [...this.scores, this.emptyScore()];
    this.teams = [...this.teams];
  }

  emptyScore(): ScoreDto {
    return {
      id: null,
      created: null,
      eventId: null,
      playerId: null,
      playerCategory: null,
      clubId: null,
      score: null,
    };
  }

  onCreate(score: ScoreDto, nasadenie: number): void {
    this.scoresService
      .create(new ScoreCreate(this.eventDto.id, score.playerId, score.clubId, score.playerCategory))
      .pipe(
        mergeMap(
          () =>
            nasadenie ? this.createUpdateTeam(score, nasadenie) : of(null),
          (v) => v
        ),
        take(1)
      )
      .subscribe(({ insertId }) => {
        score.id = insertId;
        this.refresh();
        this.messageService.success('Hráč bol pridaný.');
        this.changeDetectorRef.detectChanges();
      });
  }

  onDelete(event: MouseEvent, scoreDto: ScoreDto): void {
    this.confirmationService.delete(event, () => {
      this.scoresService
        .delete(scoreDto.id)
        .pipe(
          mergeMap(() => this.removeFromTeam(scoreDto)),
          take(1)
        )
        .subscribe(() => {
          this.scores = this.scores.filter((s) => s.id !== scoreDto.id);
          this.messageService.success('Hráč bol vymazaný.');
          this.changeDetectorRef.detectChanges();
        });
    });
  }

  onUpdate(event, scoreDto: ScoreDto): void {
    this.scoresService
      .updateCategory(scoreDto.id, new ScoreCategoryUpdate(event.value))
      .pipe(take(1))
      .subscribe(() => {
        this.messageService.success("Kategória hráča bola upravená.");
        this.changeDetectorRef.detectChanges();
      })
  }

  onChangePlayer(playerId: number, options: PlayerDto[]): void {
    const playerDto: PlayerDto = options.find((p) => p.id === playerId);
    this.scores[this.scores.length - 1].clubId = playerDto.clubId;
    this.scores[this.scores.length - 1].playerCategory = playerDto.category;
  }
}
