import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { SnapshotDto } from '../../../models/snapshot-dto';
import { SnapshotCreate } from '../../../models/snapshot-create';
import { Resp } from '../../../models/resp';
import { SnapshotHttpService } from './http/snapshot-http.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SnapshotService {
  constructor(private snapshotHttpService: SnapshotHttpService) {}

  public get(eventId: number): Observable<SnapshotDto> {
    return this.snapshotHttpService.get(eventId).pipe(
      catchError((r) => {
        return of(null);
      })
    );
  }

  public create(snapshotCreate: SnapshotCreate): Observable<Resp> {
    return this.snapshotHttpService.create(snapshotCreate);
  }
}
