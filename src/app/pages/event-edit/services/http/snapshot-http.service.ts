import { Injectable } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { Resp } from '../../../../models/resp';
import { SnapshotDto } from '../../../../models/snapshot-dto';
import { SnapshotCreate } from '../../../../models/snapshot-create';

@Injectable({
  providedIn: 'root',
})
export class SnapshotHttpService {
  constructor(private apiService: ApiService) {}

  public get(eventId: number): Observable<SnapshotDto> {
    return this.apiService.get({
      path: '/snapshot/' + eventId,
      endp: environment.coreEndp,
      disableErrorHandling: true,
    });
  }

  public create(snapshotCreate: SnapshotCreate): Observable<Resp> {
    return this.apiService.post(
      {
        path: '/snapshot',
        endp: environment.coreEndp,
      },
      snapshotCreate
    );
  }
}
