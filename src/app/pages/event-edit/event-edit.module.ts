import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { EventEditComponent } from './component/event-edit.component';
import { EventEditRoutingModule } from './event-edit-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { EventRoundsModule } from './modules/event-rounds/event-rounds.module';
import { EventScoresModule } from './modules/event-scores/event-scores.module';
import { ButtonModule } from 'primeng-lts/button';
import { InputTextModule } from 'primeng-lts/inputtext';
import { CalendarModule } from 'primeng-lts/calendar';
import {EventUserCreateModule} from "./modules/event-user-create/event-user-create.module";

@NgModule({
  declarations: [EventEditComponent],
  imports: [
    CommonModule,
    EventEditRoutingModule,
    ReactiveFormsModule,
    EventRoundsModule,
    EventScoresModule,
    ButtonModule,
    InputTextModule,
    CalendarModule,
    EventUserCreateModule,
  ],
  providers: [DatePipe],
})
export class EventEditModule {}
