import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventWriteComponent } from './component/event-write.component';
import { PlayersTableModule } from '../../shared/players-table/players-table.module';
import { EventWriteRoutingModule } from './event-write-routing.module';

@NgModule({
  declarations: [EventWriteComponent],
  imports: [CommonModule, PlayersTableModule, EventWriteRoutingModule],
})
export class EventWriteModule {}
