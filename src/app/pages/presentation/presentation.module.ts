import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PresentationComponent } from './component/presentation.component';
import { ButtonModule } from 'primeng-lts/button';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng-lts/inputtext';
import { OverlayPanelModule } from 'primeng-lts/overlaypanel';
import { SliderModule } from 'primeng-lts/slider';
import { CheckboxModule } from 'primeng-lts/checkbox';

@NgModule({
  declarations: [PresentationComponent],
  imports: [
    CommonModule,
    ButtonModule,
    FormsModule,
    OverlayPanelModule,
    InputTextModule,
    SliderModule,
    CheckboxModule,
  ],
  exports: [PresentationComponent],
})
export class PresentationModule {}
