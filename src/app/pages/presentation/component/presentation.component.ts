import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { PresentationService } from '../../../services/presentation.service';
import { resultCategories } from '../../../models/result-category';
import { Presentation } from '../../../models/presentation';
import { text } from '../../../constants/text';
import { MenuAddonsService } from '../../../services/menu-addons.service';
import { MenuButton } from '../../../models/menu-button';
import { OverlayPanel } from 'primeng-lts/overlaypanel';
import { EventDto } from '../../../models/event-dto';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PresentationComponent implements OnInit, AfterViewInit, OnDestroy {
  presentations: Presentation[] = resultCategories.map(
    (category) => new Presentation(category, 10, true)
  );
  presentationSpeed: number = 50;
  category = text.category;

  static BTN_ID = 'prezentacia-btn';

  @Input() eventDto: EventDto;
  @ViewChild(OverlayPanel) overlayPanel: OverlayPanel;

  constructor(
    public presentationService: PresentationService,
    private menuAddonsService: MenuAddonsService
  ) {}

  ngOnInit(): void {
    const prez = this.presentationService.getLocalStorage();
    if (prez) {
      this.presentations = prez.presentations;
      this.presentationSpeed = prez.speed;
    }
  }

  ngAfterViewInit(): void {
    this.menuAddonsService.setButtons([
      new MenuButton(
        PresentationComponent.BTN_ID,
        this.overlayPanel.toggle.bind(this.overlayPanel),
        'Prezentácia',
        'pi pi-video',
        '',
        true
      ),
    ]);
  }

  onCheckboxChange(v: boolean, index: number): void {
    if (!v) this.presentations[index].seconds = 0;
    else this.presentations[index].seconds = 10;
  }

  onSlideEnd(v: number, index: number): void {
    this.presentations[index].checked = v !== 0;
  }

  onStart(): void {
    this.overlayPanel.hide();
    this.presentationService.startPresentation(
      this.eventDto,
      this.presentations,
      this.presentationSpeed
    );
  }

  ngOnDestroy() {
    this.menuAddonsService.setButtons([]);
  }
}
