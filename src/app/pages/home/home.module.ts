import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './component/home.component';
import { HomeRoutingModule } from './home-routing.module';
import {CupsTableModule} from "../cups/modules/cups-table/cups-table.module";
import {CupTableModule} from "../cups/modules/cup-table/cup-table.module";

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, HomeRoutingModule, CupsTableModule, CupTableModule],
})
export class HomeModule {}
