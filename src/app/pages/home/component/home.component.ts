import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CupsService } from '../../cups/services/cups.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent implements OnInit {
  constructor(public cupsService: CupsService) {}

  ngOnInit(): void {}
}
