import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { PlayerDto } from '../../../../../models/player-dto';
import { PlayersService } from '../../../services/players.service';
import {
  playerCategories,
  PlayerCategory,
} from '../../../../../models/player-category';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClubsService } from '../../../../clubs/services/clubs.service';
import { take } from 'rxjs/operators';
import { ConfirmationService } from '../../../../../services/confirmation.service';

@Component({
  selector: 'app-player-dialog',
  templateUrl: './player-dialog.component.html',
  styleUrls: ['./player-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayerDialogComponent implements OnInit {
  showDialog: boolean = true;
  playerCategories: PlayerCategory[] = playerCategories;

  formGroup: FormGroup = this.fb.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    category: ['', Validators.required],
    clubId: [null],
  });

  @Input() selectedPlayer: PlayerDto;

  constructor(
    private playersService: PlayersService,
    private fb: FormBuilder,
    public clubsService: ClubsService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    if (this.selectedPlayer.id) {
      this.patchFormGroup(this.selectedPlayer);
    }
  }

  private patchFormGroup(selectedPlayer: PlayerDto): void {
    this.formGroup.patchValue({
      firstname: selectedPlayer.firstname,
      lastname: selectedPlayer.lastname,
      category: selectedPlayer.category,
      clubId: selectedPlayer.clubId,
    });
  }

  onClose(): void {
    this.playersService.setSelectedPlayer(null);
  }

  onSubmit(event: MouseEvent): void {
    if (this.formGroup.valid) {
      if (this.selectedPlayer.id) {
        this.confirmationService.update(event, () => {
          this.playersService
            .update(this.selectedPlayer.id, this.formGroup.value)
            .pipe(take(1))
            .subscribe(() => {
              this.onClose();
            });
        });
      } else {
        this.playersService
          .create(this.formGroup.value)
          .pipe(take(1))
          .subscribe(() => {
            this.onClose();
          });
      }
    }
  }

  onDelete(event: MouseEvent): void {
    this.confirmationService.delete(event, () => {
      this.playersService
        .delete(this.selectedPlayer.id)
        .pipe(take(1))
        .subscribe(() => {
          this.onClose();
        });
    });
  }
}
