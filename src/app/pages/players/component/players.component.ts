import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { PlayersService } from '../services/players.service';
import { ClubsService } from '../../clubs/services/clubs.service';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayersComponent implements OnInit {
  constructor(
    public playersService: PlayersService,
    public clubsService: ClubsService
  ) {}

  ngOnInit(): void {}
}
