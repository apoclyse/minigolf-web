import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayersComponent } from './component/players.component';
import { PlayersRoutingModule } from './players-routing.module';
import { PlayersTableModule } from "./modules/players-table/players-table.module";
import {PlayerDialogModule} from "./modules/player-dialog/player-dialog.module";

@NgModule({
  declarations: [PlayersComponent],
  imports: [CommonModule, PlayersRoutingModule, PlayersTableModule, PlayerDialogModule]
})
export class PlayersModule {}
