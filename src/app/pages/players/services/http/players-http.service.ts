import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PlayerDto } from '../../../../models/player-dto';
import { ApiService } from '../../../../services/api.service';
import { environment } from '../../../../../environments/environment';
import { Empty } from '../../../../models/empty';
import { PlayerCreateUpdate } from '../../../../models/player-create-update';

@Injectable({
  providedIn: 'root',
})
export class PlayersHttpService {
  constructor(private apiService: ApiService) {}

  public getAll(): Observable<PlayerDto[]> {
    return this.apiService.get({
      path: '/players',
      endp: environment.coreEndp,
    });
  }

  public getByEventId(eventId: number): Observable<PlayerDto[]> {
    return this.apiService.get({
      path: '/players/' + eventId,
      endp: environment.coreEndp,
    });
  }

  public get(id: number): Observable<PlayerDto> {
    return this.apiService.get({
      path: '/player/' + id,
      endp: environment.coreEndp,
    });
  }

  public create(playerCreateUpdate: PlayerCreateUpdate): Observable<Empty> {
    return this.apiService.post(
      {
        path: '/players',
        endp: environment.coreEndp,
      },
      playerCreateUpdate
    );
  }

  public update(
    id: number,
    playerCreateUpdate: PlayerCreateUpdate
  ): Observable<Empty> {
    return this.apiService.put(
      {
        path: '/player/' + id,
        endp: environment.coreEndp,
      },
      playerCreateUpdate
    );
  }

  public delete(id: number): Observable<Empty> {
    return this.apiService.delete({
      path: '/player/' + id,
      endp: environment.coreEndp,
    });
  }
}
