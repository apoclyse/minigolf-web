import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { PlayerDto } from '../../../models/player-dto';
import { PlayersHttpService } from './http/players-http.service';
import { Empty } from '../../../models/empty';
import { PlayerCreateUpdate } from '../../../models/player-create-update';
import { catchError, shareReplay, take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PlayersService {
  private players: BehaviorSubject<PlayerDto[]> = new BehaviorSubject<
    PlayerDto[]
  >(undefined);
  private selectedPlayer: BehaviorSubject<PlayerDto> =
    new BehaviorSubject<PlayerDto>(null);
  private loading: boolean;

  private cache: Map<number, Observable<PlayerDto>> = new Map<
    number,
    Observable<PlayerDto>
  >();

  constructor(private playersHttpService: PlayersHttpService) {}

  get players$(): Observable<PlayerDto[]> {
    if (this.players.value || this.loading) {
      return this.players.asObservable();
    } else {
      this.reset();
      return this.players.asObservable();
    }
  }

  get selectedPlayer$(): Observable<PlayerDto> {
    return this.selectedPlayer.asObservable();
  }

  public reset(): void {
    this.loading = true;
    this.cache.clear();
    this.getAll()
      .pipe(take(1))
      .subscribe(() => {
        this.loading = false;
      });
  }

  public getAll(): Observable<PlayerDto[]> {
    return this.playersHttpService.getAll().pipe(
      tap((playerDtos) => {
        this.setCache(playerDtos);
        this.players.next(playerDtos);
      })
    );
  }

  public getByEventId(eventId: number): Observable<PlayerDto[]> {
    return this.playersHttpService.getByEventId(eventId).pipe(
      tap((playerDtos) => {
        this.setCache(playerDtos);
      })
    );
  }

  public getCached(id: number): Observable<PlayerDto> {
    if (this.cache.has(id)) {
      return this.cache.get(id);
    }
    const playerDto$ = this.get(id).pipe(
      shareReplay(),
      catchError((error) => {
        this.cache.delete(id);
        return of(error);
      })
    );
    this.cache.set(id, playerDto$);
    return playerDto$;
  }

  public get(id: number): Observable<PlayerDto> {
    return this.playersHttpService.get(id);
  }

  public create(playerCreateUpdate: PlayerCreateUpdate): Observable<Empty> {
    return this.playersHttpService
      .create(playerCreateUpdate)
      .pipe(tap(() => this.reset()));
  }

  public update(
    id: number,
    playerCreateUpdate: PlayerCreateUpdate
  ): Observable<Empty> {
    return this.playersHttpService
      .update(id, playerCreateUpdate)
      .pipe(tap(() => this.reset()));
  }

  public delete(id: number): Observable<Empty> {
    return this.playersHttpService.delete(id).pipe(tap(() => this.reset()));
  }

  setSelectedPlayer(playerDto: PlayerDto): void {
    this.selectedPlayer.next(playerDto);
  }

  private setCache(playerDtos: PlayerDto[]): void {
    playerDtos.forEach((playerDto) => {
      this.cache.set(playerDto.id, of(playerDto));
    });
  }
}
