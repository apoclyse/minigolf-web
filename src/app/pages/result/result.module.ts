import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultComponent } from './component/result.component';
import { ResultRoutingModule } from './result-routing.module';
import { ClubsTableModule } from '../../shared/clubs-table/clubs-table.module';
import { PlayersTableModule } from '../../shared/players-table/players-table.module';
import {PresentationModule} from "../presentation/presentation.module";

@NgModule({
  declarations: [ResultComponent],
  imports: [
    CommonModule,
    ResultRoutingModule,
    ClubsTableModule,
    PlayersTableModule,
    PresentationModule,
  ],
})
export class ResultModule {}
