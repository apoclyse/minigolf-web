import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { merge, Observable, of, throwError } from 'rxjs';
import { EventDto } from '../../../models/event-dto';
import { map, mergeMap, tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../../../services/events.service';
import {
  resultCategories,
  ResultCategory,
} from '../../../models/result-category';
import { PresentationService } from '../../../services/presentation.service';
import { Routes } from '../../../constants/routes';
import { Score } from '../../../models/score';
import { MenuAddonsService } from '../../../services/menu-addons.service';
import { MenuDropdown } from '../../../models/menu-dropdown';
import { SelectItem } from 'primeng-lts/api';
import { text } from '../../../constants/text';
import { LoginService } from '../../login/services/login.service';
import { PlayersTableComponent } from '../../../shared/players-table/component/players-table.component';
import { ClubsTableComponent } from '../../../shared/clubs-table/component/clubs-table.component';
import { EventState } from '../../../models/event-state';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultComponent implements OnInit, OnDestroy {
  event$: Observable<EventDto> = this.eventsService
    .get(Number(this.activatedRoute.snapshot.paramMap.get('id')))
    .pipe(
      mergeMap((eventDto) =>
        eventDto.state === EventState.AVAILABLE
          ? of(eventDto)
          : throwError('turnaj nie je sprístupnený')
      ),
      tap((event) => {
        this.selectedEvent = event;
      })
    );

  resultCategory$: Observable<ResultCategory> = merge(
    this.activatedRoute.paramMap.pipe(
      map((params) => {
        return this.getCategory(
          params.get('category').toUpperCase() as ResultCategory
        );
      })
    ),
    this.presentationService.category$.pipe(
      tap((cat) => {
        cat === ResultCategory.TEAMS
          ? this.clubsTableComponent?.resetTeams()
          : this.playersTableComponent?.resetScores();
      })
    )
  );

  selectedEvent: EventDto;
  resultCategory = ResultCategory;
  resultCategories: SelectItem[] = resultCategories.map((v) => ({
    label: text.category[v],
    value: v,
  }));

  @ViewChild(PlayersTableComponent)
  playersTableComponent: PlayersTableComponent;

  @ViewChild(ClubsTableComponent) clubsTableComponent: ClubsTableComponent;

  constructor(
    private activatedRoute: ActivatedRoute,
    private eventsService: EventsService,
    private router: Router,
    public presentationService: PresentationService,
    public menuAddonsService: MenuAddonsService,
    public loginService: LoginService
  ) {}

  ngOnInit(): void {
    this.menuAddonsService.setMenuDropdown(
      new MenuDropdown(this.onChangeCategory.bind(this), this.resultCategories)
    );
  }

  onChangeCategory(cat: ResultCategory): void {
    this.router.navigate([
      `${Routes.RESULT}${this.selectedEvent.id}${Routes[cat]}`,
    ]);
  }

  onSelectPlayer(score: Score): void {
    this.router.navigate([`${Routes.RESULT_PLAYER}/${score.id}`]);
  }

  getCategory(resultCategory: ResultCategory): ResultCategory {
    return ResultCategory[resultCategory] ?? ResultCategory.ALL;
  }

  ngOnDestroy() {
    this.menuAddonsService.setMenuDropdown(null);
  }
}
