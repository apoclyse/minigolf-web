import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResultComponent } from './component/result.component';

const routes: Routes = [
  { path: ':id', redirectTo: ':id/all', pathMatch: 'full' },
  { path: ':id/:category', component: ResultComponent },
  {
    path: '**',
    loadChildren: () =>
      import('../../pages/http-forbidden/http-forbidden.module').then(
        (m) => m.HttpForbiddenModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResultRoutingModule {}
