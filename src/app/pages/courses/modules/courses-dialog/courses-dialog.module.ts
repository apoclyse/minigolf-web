import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesDialogComponent } from './component/courses-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from 'primeng-lts/dialog';
import { ButtonModule } from 'primeng-lts/button';
import { InputTextModule } from 'primeng-lts/inputtext';
import { AutoCompleteModule } from 'primeng-lts/autocomplete';
import { TableModule } from 'primeng-lts/table';

@NgModule({
  declarations: [CoursesDialogComponent],
  exports: [CoursesDialogComponent],
  imports: [
    CommonModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    ReactiveFormsModule,
    AutoCompleteModule,
    TableModule,
  ],
})
export class CoursesDialogModule {}
