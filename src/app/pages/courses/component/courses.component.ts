import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CourseService} from "../services/course.service";

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoursesComponent implements OnInit {

  constructor(public courseService: CourseService) { }

  ngOnInit(): void {
  }

}
