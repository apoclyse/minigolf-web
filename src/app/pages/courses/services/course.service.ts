import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, shareReplay, take, tap } from 'rxjs/operators';
import { Empty } from '../../../models/empty';
import { CourseDto } from '../../../models/course-dto';
import { CourseHttpService } from './http/course-http.service';
import { CourseCreateUpdate } from '../../../models/course-create-update';

@Injectable({
  providedIn: 'root',
})
export class CourseService {
  private courses: BehaviorSubject<CourseDto[]> = new BehaviorSubject<
    CourseDto[]
  >(undefined);
  private selectedCourse: BehaviorSubject<CourseDto> =
    new BehaviorSubject<CourseDto>(null);
  private loading: boolean;

  private cache: Map<number, Observable<CourseDto>> = new Map<
    number,
    Observable<CourseDto>
  >();

  constructor(private courseHttpService: CourseHttpService) {}

  get courses$(): Observable<CourseDto[]> {
    if (this.courses.value || this.loading) {
      return this.courses.asObservable();
    } else {
      this.reset();
      return this.courses.asObservable();
    }
  }

  get selectedCourse$(): Observable<CourseDto> {
    return this.selectedCourse.asObservable();
  }

  public reset(): void {
    this.loading = true;
    this.cache.clear();
    this.getAll()
      .pipe(take(1))
      .subscribe(() => {
        this.loading = false;
      });
  }

  public getAll(): Observable<CourseDto[]> {
    return this.courseHttpService.getAll().pipe(
      tap((courseDtos) => {
        this.setCache(courseDtos);
        this.courses.next(courseDtos);
      })
    );
  }

  public getCached(id: number): Observable<CourseDto> {
    if (this.cache.has(id)) {
      return this.cache.get(id);
    }
    const courseDto$ = this.get(id).pipe(
      shareReplay(),
      catchError((error) => {
        this.cache.delete(id);
        return of(error);
      })
    );
    this.cache.set(id, courseDto$);
    return courseDto$;
  }

  public get(id: number): Observable<CourseDto> {
    return this.courseHttpService.get(id);
  }

  public create(courseCreateUpdate: CourseCreateUpdate): Observable<Empty> {
    return this.courseHttpService
      .create(courseCreateUpdate)
      .pipe(tap(() => this.reset()));
  }

  public update(
    id: number,
    courseCreateUpdate: CourseCreateUpdate
  ): Observable<Empty> {
    return this.courseHttpService
      .update(id, courseCreateUpdate)
      .pipe(tap(() => this.reset()));
  }

  public delete(id: number): Observable<Empty> {
    return this.courseHttpService.delete(id).pipe(tap(() => this.reset()));
  }

  setSelectedCourse(courseDto: CourseDto): void {
    this.selectedCourse.next(courseDto);
  }

  private setCache(courseDtos: CourseDto[]): void {
    courseDtos.forEach((courseDto) => {
      this.cache.set(courseDto.id, of(courseDto));
    });
  }
}
