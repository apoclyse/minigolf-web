import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesComponent } from './component/courses.component';
import { CoursesRoutingModule } from './courses-routing.module';
import {CoursesTableModule} from "./modules/courses-table/courses-table.module";
import {CoursesDialogModule} from "./modules/courses-dialog/courses-dialog.module";

@NgModule({
  declarations: [CoursesComponent],
  imports: [CommonModule, CoursesRoutingModule, CoursesTableModule, CoursesDialogModule],
})
export class CoursesModule {}
