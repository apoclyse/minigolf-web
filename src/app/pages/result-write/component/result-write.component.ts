import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ScoresService } from '../../../services/scores.service';
import { RoundsService } from '../../../services/rounds.service';
import { Observable } from 'rxjs';
import { Score } from '../../../models/score';
import { shareReplay, switchMap, take, tap } from 'rxjs/operators';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from '../../../services/message.service';
import { InputNumber } from 'primeng-lts/inputnumber';
import { ScoreUpdate } from '../../../models/score-update';
import { Round } from '../../../models/round';
import { lanesIndex } from '../../courses/constants/lanesIndex';

@Component({
  selector: 'app-result-write',
  templateUrl: './result-write.component.html',
  styleUrls: ['./result-write.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultWriteComponent {
  scoreWithRound$: Observable<{ score: Score; rounds: Round[] }> =
    this.getScoreWithRounds();

  formGroup: FormGroup = this.fb.group({
    id: [null, Validators.required],
    rounds: this.fb.array([]),
  });

  @ViewChildren('numberInput') numberInputs: QueryList<InputNumber>;

  get roundsArray(): FormArray {
    return this.formGroup.get('rounds') as FormArray;
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    public scoresService: ScoresService,
    private roundsService: RoundsService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  private getScoreWithRounds(): Observable<{ score: Score; rounds: Round[] }> {
    return this.scoresService
      .getScore(Number(this.activatedRoute.snapshot.paramMap.get('id')))
      .pipe(
        switchMap(
          (score) =>
            this.roundsService.getRounds(score.eventId).pipe(
              tap((rounds) => {
                this.patchFormGroup(score, rounds);
              })
            ),
          (score, rounds) => ({ score, rounds })
        ),
        shareReplay()
      );
  }

  private patchFormGroup(score: Score, rounds: Round[]): void {
    this.formGroup.controls.id.setValue(score.id);
    this.roundsArray.clear();
    rounds.forEach((round, i) => {
      this.roundsArray.push(
        this.fb.group(
          score.rounds[i]?.score.map((s) =>
            this.fb.control(s || null, [Validators.min(1), Validators.max(7)])
          ) ??
            lanesIndex.map((lane) =>
              this.fb.control(null, [Validators.min(1), Validators.max(7)])
            )
        )
      );
    });
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      this.scoresService
        .update(
          this.formGroup.value.id,
          new ScoreUpdate(
            Object.values(
              this.roundsArray.value.map((o) =>
                Object.values(o).map((v) => v || 0)
              )
            )
          )
        )
        .pipe(take(1))
        .subscribe(() => {
          this.messageService.success('Úspešne uložené');
          this.formGroup.markAsPristine();
          this.onReloadScore();
          this.changeDetectorRef.detectChanges();
        });
    }
  }

  onInput(roundIdx: number, rowIndex: number): void {
    if (rowIndex > 16) document.getElementById('save-button').focus();

    this.numberInputs
      .find(
        (numberInput) => numberInput.inputId === `${roundIdx}${rowIndex + 1}`
      )
      ?.input.nativeElement.focus();
  }

  onReloadScore(): void {
    this.scoreWithRound$ = this.getScoreWithRounds();
  }
}
