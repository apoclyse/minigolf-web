import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResultWriteComponent } from './component/result-write.component';

const routes: Routes = [
  { path: ':id', component: ResultWriteComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResultWriteRoutingModule {}
