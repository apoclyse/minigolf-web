import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { Login } from '../../model/login';
import { LoginDto } from '../../model/login-dto';
import { ApiService } from '../../../../services/api.service';
import { Empty } from '../../../../models/empty';
import { UserCreate } from '../../model/user-create';

@Injectable({
  providedIn: 'root',
})
export class LoginHttpService {
  constructor(private apiService: ApiService) {}

  public getLogin(): Observable<LoginDto> {
    return this.apiService.get({
      path: '/login',
      endp: environment.coreEndp,
    });
  }

  public login(login: Login): Observable<LoginDto> {
    return this.apiService.post(
      {
        path: '/login',
        endp: environment.coreEndp,
      },
      login
    );
  }

  public createUser(userCreate: UserCreate): Observable<Empty> {
    return this.apiService.post(
      {
        path: '/user',
        endp: environment.coreEndp,
      },
      userCreate
    );
  }
}
