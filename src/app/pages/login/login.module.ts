import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './component/login.component';
import {LoginRoutingModule} from './login-routing.module';
import {InputTextModule} from "primeng-lts/inputtext";
import {ReactiveFormsModule} from "@angular/forms";
import {ButtonModule} from "primeng-lts/button";

@NgModule({
  declarations: [LoginComponent],
  imports: [CommonModule, LoginRoutingModule, InputTextModule, ReactiveFormsModule, ButtonModule],
})
export class LoginModule {}
