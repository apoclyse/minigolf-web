export enum Role {
  ADMIN = "ADMIN",
  OWNER = "OWNER",
  USER = "USER",
}
