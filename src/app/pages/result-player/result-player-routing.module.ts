import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResultPlayerComponent } from './component/result-player.component';

const routes: Routes = [
  { path: ':id', component: ResultPlayerComponent },
  {
    path: '**',
    loadChildren: () =>
      import('../../pages/http-forbidden/http-forbidden.module').then(
        (m) => m.HttpForbiddenModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResultPlayerRoutingModule {}
