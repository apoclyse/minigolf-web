import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { EventsService } from '../../../services/events.service';
import { Routes } from '../../../constants/routes';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { EventDto } from '../../../models/event-dto';
import { MenuButton } from '../../../models/menu-button';
import { MenuAddonsService } from '../../../services/menu-addons.service';
import { LoginService } from '../../login/services/login.service';
import { Role } from '../../login/model/role';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsComponent implements OnInit, OnDestroy {
  showDialog: boolean;
  events$: Observable<EventDto[]> = this.eventsService.getAll();

  constructor(
    public eventsService: EventsService,
    private router: Router,
    private menuAddonsService: MenuAddonsService,
    private changeDetectorRef: ChangeDetectorRef,
    public loginService: LoginService
  ) {}

  ngOnInit(): void {
    if (
      this.loginService.loginValue.role === Role.ADMIN ||
      this.loginService.loginValue.role === Role.OWNER
    ) {
      this.menuAddonsService.setButtons([
        new MenuButton(
          'events-btn',
          this.onOpenDialog.bind(this),
          'Pridať turnaj',
          'pi pi-plus',
          ''
        ),
      ]);
    }
  }

  onOpenDialog(): void {
    this.showDialog = true;
    this.changeDetectorRef.detectChanges();
  }

  onCloseDialog(updateView?: boolean): void {
    this.showDialog = false;
    if (updateView) {
      this.events$ = this.eventsService.getAll();
    }
  }

  onEventWrite(eventId: number): void {
    this.router.navigate([Routes.EVENT_WRITE + eventId]);
  }

  onEventEdit(eventId: number): void {
    this.router.navigate([Routes.EVENT_EDIT + eventId]);
  }

  ngOnDestroy() {
    this.menuAddonsService.setButtons([]);
  }
}
