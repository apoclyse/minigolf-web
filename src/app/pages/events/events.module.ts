import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsComponent } from './component/events.component';
import { EventsRoutingModule } from './events-routing.module';
import { EventsCreateDialogModule } from './modules/events-create-dialog/events-create-dialog.module';
import { ButtonModule } from 'primeng-lts/button';
import { EventsTableModule } from '../../shared/events-table/events-table.module';
import { TooltipModule } from 'primeng-lts/tooltip';
import { HasPermissionsPipe } from './pipes/has-permissions.pipe';

@NgModule({
  declarations: [EventsComponent, HasPermissionsPipe],
  imports: [
    CommonModule,
    EventsRoutingModule,
    EventsTableModule,
    EventsCreateDialogModule,
    ButtonModule,
    TooltipModule,
  ],
})
export class EventsModule {}
