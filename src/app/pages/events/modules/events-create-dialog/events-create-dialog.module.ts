import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsCreateDialogComponent } from './component/events-create-dialog.component';
import { DialogModule } from 'primeng-lts/dialog';
import { ButtonModule } from 'primeng-lts/button';
import { InputTextModule } from 'primeng-lts/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng-lts/calendar';

@NgModule({
  declarations: [EventsCreateDialogComponent],
  imports: [
    CommonModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    ReactiveFormsModule,
    CalendarModule,
  ],
  exports: [EventsCreateDialogComponent],
})
export class EventsCreateDialogModule {}
