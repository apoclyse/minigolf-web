import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpForbiddenComponent } from './component/http-forbidden.component';

const routes: Routes = [{ path: '', component: HttpForbiddenComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HttpForbiddenRoutingModule {}
