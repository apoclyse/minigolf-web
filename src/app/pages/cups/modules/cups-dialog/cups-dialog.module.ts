import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CupsDialogComponent } from './component/cups-dialog.component';
import { DialogModule } from 'primeng-lts/dialog';
import { ButtonModule } from 'primeng-lts/button';
import { InputTextModule } from 'primeng-lts/inputtext';
import { DropdownModule } from 'primeng-lts/dropdown';
import { ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng-lts/table';

@NgModule({
  declarations: [CupsDialogComponent],
  exports: [CupsDialogComponent],
  imports: [
    CommonModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    ReactiveFormsModule,
    TableModule,
  ],
})
export class CupsDialogModule {}
