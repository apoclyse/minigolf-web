import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {CupDto} from "../../../../../models/cup-dto";
import {CupsService} from "../../../services/cups.service";

@Component({
  selector: 'app-cup-table',
  templateUrl: './cup-table.component.html',
  styleUrls: ['./cup-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CupTableComponent implements OnInit {
  @Input() cupDto: CupDto;

  constructor(private cupsService: CupsService) { }

  ngOnInit(): void {
  }

  onSelectCup(selectedCup?: CupDto): void {
    this.cupsService.setSelectedCup(selectedCup ?? new CupDto());
  }
}
