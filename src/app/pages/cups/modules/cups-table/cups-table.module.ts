import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CupsTableComponent } from './component/cups-table.component';
import { CupTableModule } from '../cup-table/cup-table.module';
import { ButtonModule } from 'primeng-lts/button';

@NgModule({
  declarations: [CupsTableComponent],
  exports: [CupsTableComponent],
  imports: [CommonModule, CupTableModule, ButtonModule],
})
export class CupsTableModule {}
