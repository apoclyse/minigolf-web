import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {take} from "rxjs/operators";
import {CupsService} from "../../../services/cups.service";
import {CupDto} from "../../../../../models/cup-dto";

@Component({
  selector: 'app-cups-table',
  templateUrl: './cups-table.component.html',
  styleUrls: ['./cups-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CupsTableComponent implements OnInit {
  constructor(public cupsService: CupsService) {}

  ngOnInit(): void {
  }

  onSelectCup(selectedCup?: CupDto): void {
    this.cupsService.setSelectedCup(selectedCup ?? new CupDto());
  }
}
