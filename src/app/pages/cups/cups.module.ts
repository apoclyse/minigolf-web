import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CupsComponent } from './component/cups.component';
import { CupsTableModule } from './modules/cups-table/cups-table.module';
import { CupsDialogModule } from './modules/cups-dialog/cups-dialog.module';
import { CupsRoutingModule } from './cups-routing.module';
import { ButtonModule } from 'primeng-lts/button';

@NgModule({
  declarations: [CupsComponent],
  imports: [
    CommonModule,
    CupsRoutingModule,
    CupsTableModule,
    CupsDialogModule,
    ButtonModule,
  ],
})
export class CupsModule {}
