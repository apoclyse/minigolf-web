import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { CupsService } from '../services/cups.service';
import { CupDto } from '../../../models/cup-dto';
import { ClubsService } from '../../clubs/services/clubs.service';
import { MenuAddonsService } from '../../../services/menu-addons.service';
import { MenuButton } from '../../../models/menu-button';

@Component({
  selector: 'app-cups',
  templateUrl: './cups.component.html',
  styleUrls: ['./cups.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CupsComponent implements OnInit, OnDestroy {
  constructor(
    public cupsService: CupsService,
    public clubsService: ClubsService,
    private menuAddonsService: MenuAddonsService
  ) {}

  ngOnInit(): void {
    this.menuAddonsService.setButtons([
      new MenuButton(
        'cups-btn',
        this.onSelectCup.bind(this),
        'Pridať ligu',
        'pi pi-plus',
        ''
      ),
    ]);
  }

  onSelectCup(selectedCup?: CupDto): void {
    this.cupsService.setSelectedCup(selectedCup ?? new CupDto());
  }

  ngOnDestroy() {
    this.menuAddonsService.setButtons([]);
  }
}
