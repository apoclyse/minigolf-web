import { Injectable } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { Empty } from '../../../../models/empty';
import { CupDto } from '../../../../models/cup-dto';
import { CupCreateUpdate } from '../../../../models/cup-create-update';

@Injectable({
  providedIn: 'root',
})
export class CupsHttpService {
  constructor(private apiService: ApiService) {}

  public getAll(): Observable<CupDto[]> {
    return this.apiService.get({
      path: '/cups',
      endp: environment.coreEndp,
    });
  }

  public get(id: number): Observable<CupDto> {
    return this.apiService.get({
      path: '/cup',
      endp: environment.coreEndp,
    });
  }

  public create(cupCreateUpdate: CupCreateUpdate): Observable<Empty> {
    return this.apiService.post(
      {
        path: '/cups',
        endp: environment.coreEndp,
      },
      cupCreateUpdate
    );
  }

  public update(
    id: number,
    cupCreateUpdate: CupCreateUpdate
  ): Observable<Empty> {
    return this.apiService.put(
      {
        path: '/cup/' + id,
        endp: environment.coreEndp,
      },
      cupCreateUpdate
    );
  }

  public delete(id: number): Observable<Empty> {
    return this.apiService.delete({
      path: '/cup/' + id,
      endp: environment.coreEndp,
    });
  }
}
