import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { EventDto } from '../../../models/event-dto';
import { Routes } from '../../../constants/routes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultsComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  onSelectEvent(selectedEvent: EventDto): void {
    this.router.navigate([Routes.RESULT + selectedEvent.id]);
  }
}
