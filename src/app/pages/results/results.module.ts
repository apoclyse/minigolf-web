import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultsComponent } from './component/results.component';
import { ResultsRoutingModule } from './results-routing.module';
import { EventsTableModule } from '../../shared/events-table/events-table.module';

@NgModule({
  declarations: [ResultsComponent],
  imports: [CommonModule, ResultsRoutingModule, EventsTableModule],
})
export class ResultsModule {}
