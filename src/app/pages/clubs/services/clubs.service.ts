import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ClubDto } from '../../../models/club-dto';
import { catchError, shareReplay, take, tap } from 'rxjs/operators';
import { Empty } from '../../../models/empty';
import { ClubsHttpService } from './http/clubs-http.service';
import { ClubCreateUpdate } from '../../../models/club-create-update';

@Injectable({
  providedIn: 'root',
})
export class ClubsService {
  private clubs: BehaviorSubject<ClubDto[]> = new BehaviorSubject<ClubDto[]>(
    undefined
  );
  private selectedClub: BehaviorSubject<ClubDto> = new BehaviorSubject<ClubDto>(
    null
  );
  private loading: boolean;

  private cache: Map<number, Observable<ClubDto>> = new Map<
    number,
    Observable<ClubDto>
  >();

  constructor(private clubsHttpService: ClubsHttpService) {}

  get clubs$(): Observable<ClubDto[]> {
    if (this.clubs.value || this.loading) {
      return this.clubs.asObservable();
    } else {
      this.reset();
      return this.clubs.asObservable();
    }
  }

  get selectedClub$(): Observable<ClubDto> {
    return this.selectedClub.asObservable();
  }

  public reset(): void {
    this.loading = true;
    this.cache.clear();
    this.getAll()
      .pipe(take(1))
      .subscribe(() => {
        this.loading = false;
      });
  }

  public getAll(): Observable<ClubDto[]> {
    return this.clubsHttpService.getAll().pipe(
      tap((clubDtos) => {
        this.setCache(clubDtos);
        this.clubs.next(clubDtos);
      })
    );
  }

  public getCached(id: number): Observable<ClubDto> {
    if (this.cache.has(id)) {
      return this.cache.get(id);
    }
    const clubDto$ = this.get(id).pipe(
      shareReplay(),
      catchError((error) => {
        this.cache.delete(id);
        return of(error);
      })
    );
    this.cache.set(id, clubDto$);
    return clubDto$;
  }

  public get(id: number): Observable<ClubDto> {
    return this.clubsHttpService.get(id);
  }

  public create(clubCreateUpdate: ClubCreateUpdate): Observable<Empty> {
    return this.clubsHttpService
      .create(clubCreateUpdate)
      .pipe(tap(() => this.reset()));
  }

  public update(
    id: number,
    clubCreateUpdate: ClubCreateUpdate
  ): Observable<Empty> {
    return this.clubsHttpService
      .update(id, clubCreateUpdate)
      .pipe(tap(() => this.reset()));
  }

  public delete(id: number): Observable<Empty> {
    return this.clubsHttpService.delete(id).pipe(tap(() => this.reset()));
  }

  setSelectedClub(clubDto: ClubDto): void {
    this.selectedClub.next(clubDto);
  }

  private setCache(clubDtos: ClubDto[]): void {
    clubDtos.forEach((clubDto) => {
      this.cache.set(clubDto.id, of(clubDto));
    });
  }
}
