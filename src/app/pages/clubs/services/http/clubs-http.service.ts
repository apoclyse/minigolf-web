import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PlayerDto } from '../../../../models/player-dto';
import { environment } from '../../../../../environments/environment';
import { PlayerCreateUpdate } from '../../../../models/player-create-update';
import { Empty } from '../../../../models/empty';
import { ApiService } from '../../../../services/api.service';
import { ClubDto } from '../../../../models/club-dto';
import { ClubCreateUpdate } from '../../../../models/club-create-update';

@Injectable({
  providedIn: 'root',
})
export class ClubsHttpService {
  constructor(private apiService: ApiService) {}

  public getAll(): Observable<ClubDto[]> {
    return this.apiService.get({
      path: '/clubs',
      endp: environment.coreEndp,
    });
  }

  public get(id: number): Observable<ClubDto> {
    return this.apiService.get({
      path: '/club/' + id,
      endp: environment.coreEndp,
    });
  }

  public create(clubCreateUpdate: ClubCreateUpdate): Observable<Empty> {
    return this.apiService.post(
      {
        path: '/clubs',
        endp: environment.coreEndp,
      },
      clubCreateUpdate
    );
  }

  public update(
    id: number,
    clubCreateUpdate: ClubCreateUpdate
  ): Observable<Empty> {
    return this.apiService.put(
      {
        path: '/club/' + id,
        endp: environment.coreEndp,
      },
      clubCreateUpdate
    );
  }

  public delete(id: number): Observable<Empty> {
    return this.apiService.delete({
      path: '/club/' + id,
      endp: environment.coreEndp,
    });
  }
}
