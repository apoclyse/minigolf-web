import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ClubsService } from '../../../services/clubs.service';
import { ClubDto } from '../../../../../models/club-dto';
import { MenuAddonsService } from '../../../../../services/menu-addons.service';
import { MenuButton } from '../../../../../models/menu-button';

@Component({
  selector: 'app-clubs-table',
  templateUrl: './clubs-table.component.html',
  styleUrls: ['./clubs-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClubsTableComponent implements OnInit, OnDestroy {
  constructor(
    public clubsService: ClubsService,
    private menuAddonsService: MenuAddonsService
  ) {}

  ngOnInit(): void {
    this.menuAddonsService.setButtons([
      new MenuButton(
        'clubs-btn',
        this.onSelectClub.bind(this),
        'Pridať klub',
        'pi pi-plus',
        ''
      ),
    ]);
  }

  onSelectClub(selectedClub?: ClubDto): void {
    this.clubsService.setSelectedClub(selectedClub ?? new ClubDto());
  }

  ngOnDestroy() {
    this.menuAddonsService.setButtons([]);
  }
}
