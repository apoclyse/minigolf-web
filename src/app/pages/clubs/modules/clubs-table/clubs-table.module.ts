import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClubsTableComponent } from './component/clubs-table.component';
import { TableModule } from 'primeng-lts/table';
import { ButtonModule } from 'primeng-lts/button';

@NgModule({
  declarations: [ClubsTableComponent],
  imports: [CommonModule, TableModule, ButtonModule],
  exports: [ClubsTableComponent],
})
export class ClubsTableModule {}
