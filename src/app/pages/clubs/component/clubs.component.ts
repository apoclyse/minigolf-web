import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ClubsService} from "../services/clubs.service";

@Component({
  selector: 'app-clubs',
  templateUrl: './clubs.component.html',
  styleUrls: ['./clubs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClubsComponent implements OnInit {

  constructor(public clubsService: ClubsService) { }

  ngOnInit(): void {
  }

}
