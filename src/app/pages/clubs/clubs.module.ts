import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClubsComponent } from './component/clubs.component';
import { ClubsRoutingModule } from './clubs-routing.module';
import { ClubsTableModule } from './modules/clubs-table/clubs-table.module';
import { ClubsDialogModule } from './modules/clubs-dialog/clubs-dialog.module';

@NgModule({
  declarations: [ClubsComponent],
  imports: [
    CommonModule,
    ClubsRoutingModule,
    ClubsTableModule,
    ClubsDialogModule,
  ],
})
export class ClubsModule {}
