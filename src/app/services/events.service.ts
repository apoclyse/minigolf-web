import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Empty } from '../models/empty';
import { EventDto } from '../models/event-dto';
import { EventsHttpService } from './http/events-http.service';
import { EventCreateUpdate } from '../models/event-create-update';
import { Resp } from '../models/resp';
import { EventStateUpdate } from '../models/event-state-update';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class EventsService {
  constructor(private eventsHttpService: EventsHttpService) {}

  public getAll(year: String = null): Observable<EventDto[]> {
    return this.eventsHttpService
      .getAll(year)
      .pipe(map((events) => events.map((e) => this.setDate(e))));
  }

  public get(id: number): Observable<EventDto> {
    return this.eventsHttpService.get(id).pipe(map((e) => this.setDate(e)));
  }

  public create(eventCreateUpdate: EventCreateUpdate): Observable<Resp> {
    return this.eventsHttpService.create(eventCreateUpdate);
  }

  public update(
    id: number,
    eventCreateUpdate: EventCreateUpdate
  ): Observable<Empty> {
    return this.eventsHttpService.update(id, eventCreateUpdate);
  }

  public updateState(
    id: number,
    eventStateUpdate: EventStateUpdate
  ): Observable<Empty> {
    return this.eventsHttpService.updateState(id, eventStateUpdate);
  }

  public delete(id: number): Observable<Empty> {
    return this.eventsHttpService.delete(id);
  }

  public getYears(): Observable<String[]>{
    return this.eventsHttpService.getYears();
  }

  private setDate(event: EventDto): EventDto {
    event.dateFrom = event.dateFrom
      ? Date.prototype.fromString(event.dateFrom, 'dd.MM.yyyy', '.')
      : null;
    event.dateTo = event.dateTo
      ? Date.prototype.fromString(event.dateTo, 'dd.MM.yyyy', '.')
      : null;
    return event;
  }
}
