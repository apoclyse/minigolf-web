import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subject, timer } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Presentation } from '../models/presentation';
import { EventDto } from '../models/event-dto';
import { PresentationStorage } from '../models/presentation-storage';
import { ResultCategory } from '../models/result-category';

@Injectable({
  providedIn: 'root',
})
export class PresentationService implements OnDestroy {
  private destroy: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  private run: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private category: Subject<ResultCategory> = new Subject<ResultCategory>();

  index = 0;

  event: EventDto;
  presentations: Presentation[] = [];
  speed: number;

  get destroy$(): Observable<boolean> {
    return this.destroy.asObservable().pipe(filter((v) => v));
  }

  get run$(): Observable<boolean> {
    return this.run.asObservable();
  }

  get category$(): Observable<ResultCategory> {
    return this.category.asObservable();
  }

  constructor() {}

  startPresentation(
    event: EventDto,
    presentations: Presentation[],
    speed: number
  ): void {
    PresentationService.setLocalStorage(presentations, speed);

    this.event = event;
    this.presentations = presentations.filter((p) => p.seconds > 0);
    this.speed = Math.round(101 - speed);

    const listener = (e: KeyboardEvent) => {
      if (e.keyCode == 27) {
        this.ngOnDestroy();
        document.removeEventListener('keydown', listener);
      }
    };
    document.addEventListener('keydown', listener);

    this.destroy = new BehaviorSubject<boolean>(false);
    this.run.next(true);
    this.index = 0;

    this.presentate();
  }

  private presentate(): void {
    const presentation = this.presentations[this.index];
    this.category.next(presentation.category);

    if (++this.index >= this.presentations.length) {
      this.index = 0;
    }
    timer(presentation.seconds * 1000)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.pageScroll(presentation.seconds);
      });
  }

  private pageScroll(seconds: number): void {
    window.scrollBy(0, 1); // horizontal and vertical scroll increments
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
      timer(seconds * 1000)
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => {
          this.presentate();
        });
    } else {
      timer(this.speed)
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => {
          this.pageScroll(seconds);
        });
    }
  }

  getLocalStorage(): PresentationStorage {
    const prez = JSON.parse(localStorage.getItem('prez'));
    if (prez) {
      return {
        presentations: prez.presentations,
        speed: prez.speed,
      };
    }
    return null;
  }

  private static setLocalStorage(
    presentations: Presentation[],
    speed: number
  ): void {
    localStorage.setItem(
      'prez',
      JSON.stringify({
        presentations,
        speed,
      })
    );
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.complete();
    this.run.next(false);
  }
}
