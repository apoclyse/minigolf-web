import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { EventsService } from './events.service';
import { find, map, } from 'rxjs/operators';
import { Empty } from '../models/empty';
import { TeamDto } from '../models/team-dto';
import { TeamsHttpService } from './http/teams-http.service';
import { TeamCreateUpdate } from '../models/team-create-update';
import { Team } from '../models/team';
import { Score } from '../models/score';
import { ClubsService } from '../pages/clubs/services/clubs.service';
import { ScoresService } from './scores.service';
import { Resp } from '../models/resp';
import { Round } from "../models/round";

@Injectable({
  providedIn: 'root',
})
export class TeamsService {
  constructor(
    private teamsHttpService: TeamsHttpService,
    private eventsService: EventsService,
    private clubsService: ClubsService,
    private scoresService: ScoresService
  ) {}

  public getTeams(eventId: number, rounds$: Observable<Round[]>): Observable<Team[]> {
    return forkJoin({
      teamDtos: this.getAll(eventId),
      clubDtos: this.clubsService.clubs$.pipe(find((v) => !!v)),
      scores: this.scoresService.getScoresTeams(eventId, rounds$),
    }).pipe(
      map(({ teamDtos, clubDtos, scores }) =>
        teamDtos.map((teamDto) =>
          Team.fromDto(
            teamDto,
            clubDtos.find((club) => club.id === teamDto.clubId),
            Score.findByPlayerId(scores, teamDto.player1),
            Score.findByPlayerId(scores, teamDto.player2),
            Score.findByPlayerId(scores, teamDto.player3),
            Score.findByPlayerId(scores, teamDto.player4),
            Score.findByPlayerId(scores, teamDto.player5),
            Score.findByPlayerId(scores, teamDto.player6)
          )
        )
      ),
      map((scores) =>
        scores.sort((a, b) => {
          if (!isFinite(a.average) && !isFinite(b.average)) return 0;
          if (!isFinite(a.average)) return 1;
          if (!isFinite(b.average)) return -1;
          return a.average - b.average;
        })
      )
    );
  }

  public getAll(eventId: number): Observable<TeamDto[]> {
    return this.teamsHttpService.getAll(eventId);
  }

  public create(teamCreateUpdate: TeamCreateUpdate): Observable<Resp> {
    return this.teamsHttpService.create(teamCreateUpdate);
  }

  public update(
    id: number,
    teamCreateUpdate: TeamCreateUpdate
  ): Observable<Resp> {
    return this.teamsHttpService.update(id, teamCreateUpdate);
  }

  public delete(id: number): Observable<Empty> {
    return this.teamsHttpService.delete(id);
  }
}
