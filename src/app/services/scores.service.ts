import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { filter, map, switchMap } from "rxjs/operators";
import { Empty } from '../models/empty';
import { ScoreDto } from '../models/score-dto';
import { ScoresHttpService } from './http/scores-http.service';
import { ScoreCreate } from '../models/score-create';
import { ScoreUpdate } from '../models/score-update';
import { EventsService } from './events.service';
import { PlayersService } from '../pages/players/services/players.service';
import { Score } from '../models/score';
import { PenaltyService } from './penalty.service';
import { Resp } from '../models/resp';
import {ScoreCategoryUpdate} from "../models/score-category-update";
import { Round } from "../models/round";

@Injectable({
  providedIn: 'root',
})
export class ScoresService {
  constructor(
    private scoresHttpService: ScoresHttpService,
    private eventsService: EventsService,
    private playersService: PlayersService,
    private penaltyService: PenaltyService
  ) {}

  public getScores(eventId: number): Observable<Score[]> {
    return forkJoin({
      scoreDtos: this.getAll(eventId),
      penaltyDtos: this.penaltyService.getAll(eventId),
      playerDtos: this.playersService.getByEventId(eventId),
    }).pipe(
      filter((v) => v.scoreDtos.length > 0),
      map((v) =>
        v.scoreDtos.map((scoreDto) =>
          Score.fromScoreDto(
            scoreDto,
            v.playerDtos.find((player) => player.id === scoreDto.playerId),
            v.penaltyDtos.filter(
              (penalty) => penalty.playerId === scoreDto.playerId
            )
          )
        )
      )
    );
  }

  public getScoresTeams(eventId: number, rounds$: Observable<Round[]>): Observable<Score[]> {
    return forkJoin({
      scoreDtos: this.getAll(eventId),
      penaltyDtos: this.penaltyService.getAll(eventId),
      playerDtos: this.playersService.getByEventId(eventId),
      rounds: rounds$
    }).pipe(
      filter((v) => v.scoreDtos.length > 0),
      map((v) =>
        v.scoreDtos.map((scoreDto) =>
          Score.fromScoreDtoTeams(
            scoreDto,
            v.playerDtos.find((player) => player.id === scoreDto.playerId),
            v.penaltyDtos.filter(
              (penalty) => penalty.playerId === scoreDto.playerId
            ),
            v.rounds.map((round,index) => {
                if (round.includeInTeam || round.includeInTeam === undefined){
                  return index
                } else return null
            }).filter(v => v !== null)
          )
        )
      )
    );
  }

  public getScore(id: number): Observable<Score> {
    return forkJoin({
      scoreDto: this.get(id),
      penaltyDtos: this.penaltyService.get(id),
    }).pipe(
      switchMap((v) =>
        this.playersService
          .getCached(v.scoreDto.playerId)
          .pipe(
            map((playerDto) =>
              Score.fromScoreDto(v.scoreDto, playerDto, v.penaltyDtos)
            )
          )
      )
    );
  }

  public getAll(eventId: number): Observable<ScoreDto[]> {
    return this.scoresHttpService.getAll(eventId);
  }

  public get(id: number): Observable<ScoreDto> {
    return this.scoresHttpService.get(id);
  }

  public create(scoreCreate: ScoreCreate): Observable<Resp> {
    return this.scoresHttpService.create(scoreCreate);
  }

  public update(id: number, scoreUpdate: ScoreUpdate): Observable<Empty> {
    return this.scoresHttpService.update(id, scoreUpdate);
  }

  public updateCategory(id: number, scoreCategoryUpdate: ScoreCategoryUpdate): Observable<Empty> {
    return this.scoresHttpService.updateCategory(id, scoreCategoryUpdate);
  }

  public delete(id: number): Observable<Empty> {
    return this.scoresHttpService.delete(id);
  }
}
