import { Injectable } from '@angular/core';
import { ConfirmationService as CONFIRMATION_SERVICE } from 'primeng-lts/api';

@Injectable({
  providedIn: 'root',
})
export class ConfirmationService {
  constructor(private confirmationService: CONFIRMATION_SERVICE) {}

  update(event: MouseEvent, accept: () => any, reject?: () => any): void {
    this.confirmationService.confirm({
      target: event.target,
      message: 'Naozaj uložiť ?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Áno',
      rejectLabel: 'Nie',
      accept: () => {
        accept();
      },
      reject: () => {
        if (reject) {
          reject();
        }
      },
    });
  }

  delete(event: MouseEvent, accept: () => any, reject?: () => any): void {
    this.confirmationService.confirm({
      target: event.target,
      message: 'Naozaj vymazať ?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Áno',
      rejectLabel: 'Nie',
      accept: () => {
        accept();
      },
      reject: () => {
        if (reject) {
          reject();
        }
      },
    });
  }
}
