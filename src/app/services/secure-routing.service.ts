import { Injectable } from '@angular/core';
import { LoginService } from '../pages/login/services/login.service';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SecureRoutingService {
  constructor(private loginService: LoginService) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    if (route.data.secureRead) {
      return this.loginService.canRead();
    }
    return of(false);
  }
}
