import {Injectable} from '@angular/core';
import {ApiService} from '../api.service';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Empty} from '../../models/empty';
import {TeamDto} from '../../models/team-dto';
import {TeamCreateUpdate} from '../../models/team-create-update';
import {Resp} from "../../models/resp";

@Injectable({
  providedIn: 'root',
})
export class TeamsHttpService {
  constructor(private apiService: ApiService) {}

  public getAll(eventId: number): Observable<TeamDto[]> {
    return this.apiService.get({
      path: '/teams/' + eventId,
      endp: environment.coreEndp,
    });
  }

  public create(teamCreateUpdate: TeamCreateUpdate): Observable<Resp> {
    return this.apiService.post(
      {
        path: '/teams',
        endp: environment.coreEndp,
      },
      teamCreateUpdate
    );
  }

  public update(
    id: number,
    teamCreateUpdate: TeamCreateUpdate
  ): Observable<Resp> {
    return this.apiService.put(
      {
        path: '/team/' + id,
        endp: environment.coreEndp,
      },
      teamCreateUpdate
    );
  }

  public delete(id: number): Observable<Empty> {
    return this.apiService.delete({
      path: '/team/' + id,
      endp: environment.coreEndp,
    });
  }
}
