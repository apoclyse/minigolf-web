import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Empty } from '../../models/empty';
import { ScoreDto } from '../../models/score-dto';
import { ScoreUpdate } from '../../models/score-update';
import { ScoreCreate } from '../../models/score-create';
import {Resp} from "../../models/resp";
import {ScoreCategoryUpdate} from "../../models/score-category-update";

@Injectable({
  providedIn: 'root',
})
export class ScoresHttpService {
  constructor(private apiService: ApiService) {}

  public getAll(eventId: number): Observable<ScoreDto[]> {
    return this.apiService.get({
      path: '/scores/' + eventId,
      endp: environment.coreEndp,
    });
  }

  public get(id: number): Observable<ScoreDto> {
    return this.apiService.get({
      path: '/score/' + id,
      endp: environment.coreEndp,
    });
  }

  public create(scoreCreate: ScoreCreate): Observable<Resp> {
    return this.apiService.post(
      {
        path: '/scores',
        endp: environment.coreEndp,
      },
      scoreCreate
    );
  }

  public update(id: number, scoreUpdate: ScoreUpdate): Observable<Empty> {
    return this.apiService.put(
      {
        path: '/score/' + id,
        endp: environment.coreEndp,
      },
      scoreUpdate
    );
  }

  public updateCategory(id: number, scoreCategoryUpdate: ScoreCategoryUpdate): Observable<Empty> {
    return this.apiService.put(
        {
          path: '/score/category/' + id,
          endp: environment.coreEndp,
        },
        scoreCategoryUpdate
    );
  }

  public delete(id: number): Observable<Empty> {
    return this.apiService.delete({
      path: '/score/' + id,
      endp: environment.coreEndp,
    });
  }


}
