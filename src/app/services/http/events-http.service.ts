import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Empty } from '../../models/empty';
import { EventCreateUpdate } from '../../models/event-create-update';
import { EventDto } from '../../models/event-dto';
import { Resp } from '../../models/resp';
import { EventStateUpdate } from '../../models/event-state-update';

@Injectable({
  providedIn: 'root',
})
export class EventsHttpService {
  constructor(private apiService: ApiService) {}

  public getAll(year?: String): Observable<EventDto[]> {
    return this.apiService.get({
      path: year ? `/events?year=${year}` : "/events",
      endp: environment.coreEndp,
    });
  }

  public get(id: number): Observable<EventDto> {
    return this.apiService.get({
      path: '/event/' + id,
      endp: environment.coreEndp,
    });
  }

  public create(eventCreateUpdate: EventCreateUpdate): Observable<Resp> {
    return this.apiService.post(
      {
        path: '/events',
        endp: environment.coreEndp,
      },
      eventCreateUpdate
    );
  }

  public update(
    id: number,
    eventCreateUpdate: EventCreateUpdate
  ): Observable<Empty> {
    return this.apiService.put(
      {
        path: '/event/' + id,
        endp: environment.coreEndp,
      },
      eventCreateUpdate
    );
  }

  public updateState(
    id: number,
    eventStateUpdate: EventStateUpdate
  ): Observable<Empty> {
    return this.apiService.put(
      {
        path: '/event/' + id + '/state',
        endp: environment.coreEndp,
      },
      eventStateUpdate
    );
  }

  public delete(id: number): Observable<Empty> {
    return this.apiService.delete({
      path: '/event/' + id,
      endp: environment.coreEndp,
    });
  }

  public getYears(): Observable<String[]>{
    return this.apiService.get({
      path: '/events/years',
      endp: environment.coreEndp,
    })
  }
}
