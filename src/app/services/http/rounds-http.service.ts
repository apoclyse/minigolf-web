import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Empty } from '../../models/empty';
import { RoundDto } from '../../models/round-dto';
import { RoundCreate } from '../../models/round-create';
import { RoundUpdate } from '../../models/round-update';

@Injectable({
  providedIn: 'root',
})
export class RoundsHttpService {
  constructor(private apiService: ApiService) {}

  public get(eventId: number): Observable<RoundDto> {
    return this.apiService.get({
      path: '/round/' + eventId,
      endp: environment.coreEndp,
    });
  }

  public create(roundCreate: RoundCreate): Observable<Empty> {
    return this.apiService.post(
      {
        path: '/rounds',
        endp: environment.coreEndp,
      },
      roundCreate
    );
  }

  public update(id: number, roundUpdate: RoundUpdate): Observable<Empty> {
    return this.apiService.put(
      {
        path: '/round/' + id,
        endp: environment.coreEndp,
      },
      roundUpdate
    );
  }
}
