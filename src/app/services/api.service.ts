import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { ApiParams } from '../models/api-params';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Empty } from '../models/empty';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  public get(params: ApiParams): Observable<any> {
    return this.http
      .get(params.endp + params.path)
      .pipe(
        catchError((err: any) =>
          this.handleError(err, params.disableErrorHandling)
        )
      );
  }

  public put(params: ApiParams, body: any): Observable<any> {
    return this.http
      .put(params.endp + params.path, JSON.stringify(body), httpOptions)
      .pipe(
        catchError((err: any) =>
          this.handleError(err, params.disableErrorHandling)
        )
      );
  }

  public post(params: ApiParams, body: any): Observable<any> {
    return this.http
      .post(params.endp + params.path, JSON.stringify(body), httpOptions)
      .pipe(
        catchError((err: any) =>
          this.handleError(err, params.disableErrorHandling)
        )
      );
  }

  public delete(params: ApiParams): Observable<Empty> {
    return this.http
      .delete(params.endp + params.path)
      .pipe(
        catchError((err: any) =>
          this.handleError(err, params.disableErrorHandling)
        )
      );
  }

  public deleteWithResponse<T>(params: ApiParams): Observable<T> {
    return this.http
      .delete<T>(params.endp + params.path)
      .pipe(
        catchError((err: any) =>
          this.handleError(err, params.disableErrorHandling)
        )
      );
  }

  private handleError(
    error: HttpErrorResponse,
    disableErrorHandling?: boolean
  ): Observable<never> {
    if (disableErrorHandling) return throwError(error);

    if (typeof error.error === 'string') {
      this.messageService.error(error.error);
    } else {
      this.messageService.error(JSON.stringify(error.error));
    }
    return throwError(error);
  }
}
