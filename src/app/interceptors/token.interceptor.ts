import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { LoginService } from '../pages/login/services/login.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private loginService: LoginService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = this.loginService.token;
    if (token) {
      const modifiedReq = request.clone({
        headers: request.headers.set('x-access-token', token || ''),
      });
      return next.handle(modifiedReq).pipe(
        catchError((e) => {
          if (e.code === 401 || e.status === 401) {
            this.loginService.logoutAndRedirectToLogin();
          }
          return throwError(e);
        })
      );
    }
    return next.handle(request);
  }
}
