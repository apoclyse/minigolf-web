export interface SnapshotDto {
  id: number;
  created: string;
  eventId: number;
  snapshot: string;
}
