import { Presentation } from './presentation';

export interface PresentationStorage {
  presentations: Presentation[];
  speed: number;
}
