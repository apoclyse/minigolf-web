import { ResultCategory } from './result-category';

export class Presentation {
  category: ResultCategory;
  seconds: number;
  checked: boolean;

  constructor(category: ResultCategory, seconds: number, checked: boolean) {
    this.category = category;
    this.seconds = seconds;
    this.checked = checked;
  }
}
