export enum EventState {
  AVAILABLE = 'AVAILABLE',
  UNAVAILABLE = 'UNAVAILABLE',
}

export function getNextEventState(state: EventState): EventState {
  if (state === EventState.UNAVAILABLE) return EventState.AVAILABLE;
  return EventState.UNAVAILABLE;
}
