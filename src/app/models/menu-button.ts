export class MenuButton {
  id: string;
  action: (...args) => void;
  label: string;
  icon: string;
  className: string;
  mouseEvent: boolean;

  constructor(
    id: string,
    action: (...args) => void,
    label: string,
    icon: string,
    className: string,
    mouseEvent?: boolean
  ) {
    this.id = id;
    this.action = action;
    this.label = label;
    this.icon = icon;
    this.className = className;
    this.mouseEvent = mouseEvent;
  }
}
