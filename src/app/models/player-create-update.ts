import { PlayerCategory } from './player-category';

export class PlayerCreateUpdate {
  firstname: string;
  lastname: string;
  category: PlayerCategory;
  clubId: number;

  constructor(
    firstname: string,
    lastname: string,
    category: PlayerCategory,
    clubId: number
  ) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.category = category;
    this.clubId = clubId;
  }
}
