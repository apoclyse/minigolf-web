export class TeamDto {
  id: number;
  created: string;
  eventId: number;
  clubId: number;
  player1: number;
  player2: number;
  player3: number;
  player4: number;
  player5: number;
  player6: number;
}
