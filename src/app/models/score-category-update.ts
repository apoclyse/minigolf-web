export class ScoreCategoryUpdate {
    category: string;

    constructor(category: string) {
        this.category = category;
    }
}