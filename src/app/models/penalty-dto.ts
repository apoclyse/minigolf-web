import {PenaltyType} from "./penalty-type";

export class PenaltyDto {
  id: number;
  created: string;
  eventId: number;
  roundIndex: number;
  scoreId: number;
  playerId: number;
  penalty: number;
  type: PenaltyType;
  text: string;
}
