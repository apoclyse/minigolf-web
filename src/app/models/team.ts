import { Score } from './score';
import { ClubDto } from './club-dto';
import { TeamDto } from './team-dto';
import { TeamRound } from './team-round';
import { ScoreRound } from './score-round';

export class Team {
  id: number;
  created: string;
  eventId: number;
  club: ClubDto;
  player1: Score;
  player2: Score;
  player3: Score;
  player4: Score;
  player5: Score;
  player6: Score;
  totalSum: number;
  totalPenalty: number;
  average: number;
  roundsSum: TeamRound[];

  constructor(
    id: number,
    created: string,
    eventId: number,
    club: ClubDto,
    player1: Score,
    player2: Score,
    player3: Score,
    player4: Score,
    player5: Score,
    player6: Score
  ) {
    this.id = id;
    this.created = created;
    this.eventId = eventId;
    this.club = club;
    this.player1 = player1;
    this.player2 = player2;
    this.player3 = player3;
    this.player4 = player4;
    this.player5 = player5;
    this.player6 = player6;
    this.totalPenalty = 0;

    const rounds: ScoreRound[][] = [];
    this.players.forEach((score) => {
      score.rounds.forEach((round, i) => {
        if (round.sum === 0) return;
        if (rounds[i]) {
          rounds[i].push(round);
        } else {
          rounds.push([round]);
        }
      });
    });
    if (this.players.length < 4) {
      rounds.forEach((r, i) => {
        for (let n = 0; n < 4 - this.players.length; ++n) {
          rounds[i].push(ScoreRound.fullRound());
        }
      });
    }
    this.roundsSum = rounds.map(
      (round) =>
        new TeamRound(
          round
            .filter((r) => r.sum > 0)
            .sort((a, b) => a.sum + a.penalty - (b.sum + b.penalty))
            .slice(0, 4)
            .map((r) => {
              this.totalPenalty += r.penalty;
              return r.sum;
            })
        )
    );

    this.totalSum =
      this.totalPenalty +
      this.roundsSum.reduce((sum, rs) => sum + (rs.sum ?? 0), 0);
    this.average =
      this.totalSum /
      this.roundsSum.reduce((sum, rs) => sum + (rs.roundsCount ?? 0), 0);
  }

  get players(): Score[] {
    return [1, 2, 3, 4, 5, 6]
      .map((i) => this['player' + i] as Score)
      .filter((p) => p);
  }

  static fromDto(
    teamDto: TeamDto,
    club: ClubDto,
    player1: Score,
    player2: Score,
    player3: Score,
    player4: Score,
    player5: Score,
    player6: Score
  ): Team {
    return new Team(
      teamDto.id,
      teamDto.created,
      teamDto.eventId,
      club,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6
    );
  }
}
