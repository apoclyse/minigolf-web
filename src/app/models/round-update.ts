import { Round } from './round';

export class RoundUpdate {
  rounds: Round[];

  constructor(rounds: Round[]) {
    this.rounds = rounds;
  }
}
