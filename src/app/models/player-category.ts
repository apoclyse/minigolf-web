export enum PlayerCategory {
  MJ = 'MJ',
  M = 'M',
  MS1 = 'MS1',
  MS2 = 'MS2',
  ZJ = 'ZJ',
  Z = 'Z',
  ZS = 'ZS',
}

export const playerCategories = Object.keys(PlayerCategory).map(
  (key) => PlayerCategory[key]
);
