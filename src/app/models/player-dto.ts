import {PlayerCategory} from "./player-category";

export class PlayerDto {
  id: number;
  created: string;
  firstname: string;
  lastname: string;
  category: PlayerCategory;
  clubId: number;
}
