import {PlayerCategory} from "./player-category";

export class ScoreDto {
  id: number;
  created: string;
  eventId: number;
  playerId: number;
  playerCategory: PlayerCategory;
  clubId: number;
  score: number[][];
}
