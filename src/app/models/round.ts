export class Round {
  courseId: number;
  name: string;
  includeInTeam: boolean;

  constructor(courseId: number, name: string, includeInTeam: boolean) {
    this.courseId = courseId;
    this.name = name;
    this.includeInTeam = includeInTeam;
  }
}
