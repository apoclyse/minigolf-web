import { ClubPoint } from './club-point';

export class CupDto {
  id: number;
  created: string;
  name: string;
  text: string;
  clubsIdPoints: ClubPoint[];
}
