export class TeamRound {
  sum: number;
  roundsCount: number;

  constructor(rounds: number[]) {
    this.sum = rounds.reduce((sum, roundSum) => sum + roundSum, 0);
    this.roundsCount = rounds.length;
  }
}
