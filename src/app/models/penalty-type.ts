export enum PenaltyType {
  B = 'B',
  D = 'D',
  N = 'N',
}

export const penaltyTypes: PenaltyType[] = Object.keys(PenaltyType).map((key) => PenaltyType[key]);
