import { Round } from './round';

export class RoundCreate {
  eventId: number;
  rounds: Round[];

  constructor(eventId: number, rounds: Round[]) {
    this.eventId = eventId;
    this.rounds = rounds;
  }
}
