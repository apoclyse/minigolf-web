export class ScoreCreate {
  eventId: number;
  playerId: number;
  clubId: number;
  playerCategory: string;

  constructor(eventId: number, playerId: number, clubId: number, playerCategory: string) {
    this.eventId = eventId;
    this.playerId = playerId;
    this.clubId = clubId;
    this.playerCategory = playerCategory;
  }
}
