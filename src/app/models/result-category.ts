export enum ResultCategory {
  ALL = 'ALL',
  MJ = 'MJ',
  M = 'M',
  MS1 = 'MS1',
  MS2 = 'MS2',
  ZJ = 'ZJ',
  Z = 'Z',
  ZS = 'ZS',
  TEAMS = 'TEAMS',
}

export const resultCategories: ResultCategory[] = Object.keys(
  ResultCategory
).map((key) => ResultCategory[key]);
