import {EventState} from "./event-state";

export class EventDto {
  id: number;
  created: string;
  name: string;
  organizer: string;
  dateFrom: Date;
  dateTo: Date;
  state: EventState;
}
