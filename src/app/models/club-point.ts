export class ClubPoint {
  clubId: number;
  strikes: number;
  rounds: number;
  average: number;
  points: number;
}
