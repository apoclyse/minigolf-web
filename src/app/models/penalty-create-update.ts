import { PenaltyType } from './penalty-type';

export class PenaltyCreateUpdate {
  eventId: number;
  roundIndex: number;
  scoreId: number;
  playerId: number;
  penalty: number;
  type: PenaltyType;
  text: string;

  constructor(
    eventId: number,
    roundIndex: number,
    scoreId: number,
    playerId: number,
    penalty: number,
    type: PenaltyType,
    text: string
  ) {
    this.eventId = eventId;
    this.roundIndex = roundIndex;
    this.scoreId = scoreId;
    this.playerId = playerId;
    this.penalty = penalty;
    this.type = type;
    this.text = text;
  }
}
