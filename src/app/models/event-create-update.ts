export class EventCreateUpdate {
  name: string;
  organizer: string;
  dateFrom: string;
  dateTo: string;

  constructor(name: string, organizer: string, dateFrom: string, dateTo: string) {
    this.name = name;
    this.organizer = organizer;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo === dateFrom ? null : dateTo;
  }
}
