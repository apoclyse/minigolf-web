export interface ApiParams {
  path: string;
  endp?: string;
  disableErrorHandling?: boolean;
}
