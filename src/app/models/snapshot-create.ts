export class SnapshotCreate {
  eventId: number;
  snapshot: string;

  constructor(eventId: number, snapshot: string) {
    this.eventId = eventId;
    this.snapshot = snapshot;
  }
}
