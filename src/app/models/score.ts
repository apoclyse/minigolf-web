import { PlayerDto } from './player-dto';
import { ScoreDto } from './score-dto';
import { PenaltyDto } from './penalty-dto';
import { ScoreRound } from './score-round';
import { PlayerCategory } from "./player-category";


export class Score {
  id: number;
  created: string;
  eventId: number;
  player: PlayerDto;
  category: PlayerCategory;
  clubId: number;
  rounds: ScoreRound[];
  totalSum: number;
  average: number;
  diffs: number[];
  penalty: PenaltyDto[];
  penaltySum: number;

  constructor(
    id: number,
    created: string,
    eventId: number,
    player: PlayerDto,
    category: PlayerCategory,
    clubId: number,
    rounds: ScoreRound[],
    penalty: PenaltyDto[]
  ) {
    this.id = id;
    this.created = created;
    this.eventId = eventId;
    this.player = player;
    this.category = category;
    this.clubId = clubId;
    this.rounds = rounds;
    this.penalty = penalty;
    this.penaltySum = penalty.reduce(
      (sum, penalty) => sum + (penalty.penalty ?? 0),
      0
    );
    this.totalSum =
      this.penaltySum + rounds.reduce((sum, round) => sum + round.sum, 0);
    this.average =
      this.totalSum /
      rounds.reduce((sum, round) => sum + (round.sum === 0 ? 0 : 1), 0);

    this.diffs = [];
  }

  static fromScoreDto(
    scoreDto: ScoreDto,
    player: PlayerDto,
    penaltyDtos: PenaltyDto[]
  ): Score {
    return new Score(
      scoreDto.id,
      scoreDto.created,
      scoreDto.eventId,
      player,
      scoreDto.playerCategory,
      scoreDto.clubId,
      scoreDto.score.map((s, i) =>
        ScoreRound.fromScore(s, penaltyDtos[i]?.penalty)
      ),
      penaltyDtos
    );
  }

  static fromScoreDtoTeams(
    scoreDto: ScoreDto,
    player: PlayerDto,
    penaltyDtos: PenaltyDto[],
    roundsIndex: number[]
  ): Score {
    penaltyDtos = penaltyDtos?.filter((v) => roundsIndex.includes(v.roundIndex));
    scoreDto.score = scoreDto?.score?.filter((v, i) => roundsIndex.includes(i));

    return new Score(
      scoreDto.id,
      scoreDto.created,
      scoreDto.eventId,
      player,
      scoreDto.playerCategory,
      scoreDto.clubId,
      scoreDto.score.map((s, i) =>
        ScoreRound.fromScore(s, penaltyDtos[i]?.penalty)
      ),
      penaltyDtos
    );
  }

  static findByPlayerId(scores: Score[], playerId: number): Score {
    return scores.find((score) => score.player.id === playerId);
  }
}
