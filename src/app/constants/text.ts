export const text = {
  category: {
    ALL: 'Všetci',
    MJ: 'Muži J',
    M: 'Muži',
    MS1: 'Muži S1',
    MS2: 'Muži S2',
    ZJ: 'Ženy J',
    Z: 'Ženy',
    ZS: 'Ženy S',
    TEAMS: 'Tímy',
  },
};
