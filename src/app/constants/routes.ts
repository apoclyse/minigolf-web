export const Routes = {
  EVENTS: '/events',
  EVENT_WRITE: '/event-write/',
  EVENT_EDIT: '/event-edit/',

  RESULT: '/result/',
  RESULT_PLAYER: '/result-player/',
  RESULT_WRITE: '/result-write/',

  ALL: '/all',
  MJ: '/mj',
  M: '/m',
  MS1: '/ms1',
  MS2: '/ms2',
  ZJ: '/zj',
  Z: '/z',
  ZS: '/zs',
  TEAMS: '/teams',
};
