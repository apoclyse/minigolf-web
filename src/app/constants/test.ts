import { Score } from '../models/score';
import { Round } from '../models/round';

const getScore = (id: number, sums: number[]): Score => {
  return new Score(
    1,
    null,
    null,
    null,
    null,
    sums.map((s) => ({ score: [], sum: s })),
    []
  );
};

const getRound = (courseId: number): Round => {
  return new Round(courseId, null);
};

const rounds1 = [getRound(1), getRound(1), getRound(1), getRound(1)];
const score1A: Score = getScore(1, [20, 20, 25, 25]); // diff 5, 5
const score1B: Score = getScore(2, [19, 26, 26, 19]); // diff 7, 7

export const rounds2 = [getRound(1), getRound(2), getRound(1), getRound(2)];
export const score2A: Score = getScore(1, [20, 20, 25, 25]); // diff 5, 5
export const score2B: Score = getScore(2, [19, 26, 26, 19]); // diff 7, 7

const rounds3 = [getRound(1), getRound(1), getRound(1), getRound(1)];
const score3A: Score = getScore(1, [20, 20, 25, 25]); // diff 5, 5
const score3B: Score = getScore(2, [20, 19, 26, 25]); // diff 5, 7

export const rounds4 = [
  getRound(1),
  getRound(1),
  getRound(1),
  getRound(1),
  getRound(1),
  getRound(1),
  getRound(1),
  getRound(1),
  getRound(1),
  getRound(1),
  getRound(1),
  getRound(1),
];
export const score4A: Score = getScore(
  1,
  [20, 40, 25, 50, 30, 30, 35, 35, 20, 40, 50, 25]
); // diff 30, 30, 15, 15, 5, 5
export const score4B: Score = getScore(
  2,
  [20, 50, 25, 40, 31, 29, 35, 35, 25, 40, 50, 20]
); // diff 30, 30, 15, 15, 4, 6

export const rounds5 = [
  getRound(1),
  getRound(2),
  getRound(1),
  getRound(2),
  getRound(1),
  getRound(2),
  getRound(1),
  getRound(2),
  getRound(1),
  getRound(2),
  getRound(1),
  getRound(2),
];
export const score5A: Score = getScore(
  1,
  [20, 40, 25, 30, 30, 35, 35, 20, 40, 50, 25]
); // diff ?
export const score5B: Score = getScore(
  2,
  [20, 50, 25, 40, 31, 29, 35, 35, 25, 40, 20]
); // diff ?

export const rounds6 = [getRound(1), getRound(1)];
export const score6A: Score = getScore(1, [20, 25]); // diff 5, 5
export const score6B: Score = getScore(2, [19, 19]); // diff 7, 7
