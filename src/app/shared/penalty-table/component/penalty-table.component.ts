import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { PenaltyType, penaltyTypes } from '../../../models/penalty-type';
import { SelectItem } from 'primeng-lts/api';
import { Score } from '../../../models/score';
import { Round } from '../../../models/round';
import { PenaltyService } from '../../../services/penalty.service';
import { MessageService } from '../../../services/message.service';
import { ConfirmationService } from '../../../services/confirmation.service';
import { take } from 'rxjs/operators';
import { PenaltyDto } from '../../../models/penalty-dto';
import { PenaltyCreateUpdate } from '../../../models/penalty-create-update';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-penalty-table',
  templateUrl: './penalty-table.component.html',
  styleUrls: ['./penalty-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PenaltyTableComponent implements OnInit {
  formGroup: FormGroup = this.fb.group({
    penalty: this.fb.array([]),
  });

  penaltyTypes: PenaltyType[] = penaltyTypes;
  roundsIndexed: SelectItem[];

  @Input() score: Score;
  @Input() rounds: Round[];
  @Input() readonly: boolean;
  @Output() reloadScore: EventEmitter<void> = new EventEmitter<void>();

  get penaltyArray(): FormArray {
    return this.formGroup.get('penalty') as FormArray;
  }

  constructor(
    private penaltyService: PenaltyService,
    private fb: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    if (!this.readonly) {
      this.roundsIndexed = this.rounds.map((r, i) => {
        return { value: { ...r, index: i }, label: r.name };
      });
    }
    this.patchFormGroup(this.score.penalty);
  }

  penaltyTypeChange(i: number, type: PenaltyType): void {
    if (type === PenaltyType.B) {
      this.penaltyArray.controls[i].get('penalty').enable();
    } else {
      this.penaltyArray.controls[i].get('penalty').disable();
      this.penaltyArray.controls[i].get('penalty').setValue(null);
    }
  }

  private patchFormGroup(penalty: PenaltyDto[]): void {
    this.penaltyArray.clear();
    penalty.forEach((p) =>
      this.penaltyArray.push(
        this.fb.group({
          id: [p.id, Validators.required],
          eventId: [p.eventId, Validators.required],
          roundIndex: [p.roundIndex, Validators.required],
          scoreId: [p.scoreId, Validators.required],
          playerId: [p.playerId, Validators.required],
          penalty: [
            { value: p.penalty, disabled: p.type !== PenaltyType.B },
            this.penaltyValidators(),
          ],
          type: [p.type, Validators.required],
          text: [p.text, Validators.required],
        })
      )
    );
    if (!this.readonly) {
      this.addPenalty();
    }
  }

  private addPenalty(): void {
    this.penaltyArray.push(
      this.fb.group({
        eventId: [this.score.eventId, Validators.required],
        roundIndex: [null, Validators.required],
        scoreId: [this.score.id, Validators.required],
        playerId: [this.score.player.id, Validators.required],
        penalty: [{ value: null, disabled: true }, this.penaltyValidators()],
        type: [null, Validators.required],
        text: [null, Validators.required],
      })
    );
  }

  private penaltyValidators(): ValidatorFn {
    return (control: FormControl): ValidationErrors | null =>
      control.parent?.get('type').value === PenaltyType.B
        ? {
            ...Validators.required(control),
            ...Validators.min(1)(control),
          }
        : null;
  }

  onCreate(penaltyControl: AbstractControl): void {
    if (penaltyControl.valid) {
      const penaltyControlValue: PenaltyDto = penaltyControl.value;
      this.penaltyService
        .create(
          new PenaltyCreateUpdate(
            penaltyControlValue.eventId,
            penaltyControlValue.roundIndex,
            penaltyControlValue.scoreId,
            penaltyControlValue.playerId,
            penaltyControlValue.penalty ?? 0,
            penaltyControlValue.type,
            penaltyControlValue.text
          )
        )
        .pipe(take(1))
        .subscribe(() => {
          this.messageService.success('Trest bol pridaný.');
          this.reloadScore.emit();
        });
    }
  }

  onSave(penaltyControl: AbstractControl): void {
    if (penaltyControl.valid) {
      const penaltyControlValue: PenaltyDto = penaltyControl.value;
      this.penaltyService
        .update(
          penaltyControlValue.id,
          new PenaltyCreateUpdate(
            penaltyControlValue.eventId,
            penaltyControlValue.roundIndex,
            penaltyControlValue.scoreId,
            penaltyControlValue.playerId,
            penaltyControlValue.penalty ?? 0,
            penaltyControlValue.type,
            penaltyControlValue.text
          )
        )
        .pipe(take(1))
        .subscribe(() => {
          this.messageService.success('Trest bol upravený.');
          this.reloadScore.emit();
        });
    }
  }

  onDelete(event: MouseEvent, penaltyControl: AbstractControl): void {
    this.confirmationService.delete(event, () => {
      this.penaltyService
        .delete(penaltyControl.value.id)
        .pipe(take(1))
        .subscribe(() => {
          this.messageService.success('Trest bol vymazaný.');
          this.reloadScore.emit();
        });
    });
  }
}
