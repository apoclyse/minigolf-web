import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PenaltyTableComponent } from './component/penalty-table.component';
import { TableModule } from 'primeng-lts/table';
import { InputTextModule } from 'primeng-lts/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng-lts/dropdown';
import { ButtonModule } from 'primeng-lts/button';

@NgModule({
  declarations: [PenaltyTableComponent],
  imports: [
    CommonModule,
    TableModule,
    InputTextModule,
    DropdownModule,
    ButtonModule,
    ReactiveFormsModule,
  ],
  exports: [PenaltyTableComponent],
})
export class PenaltyTableModule {}
