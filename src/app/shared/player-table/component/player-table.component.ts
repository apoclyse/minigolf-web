import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  Input,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { Score } from '../../../models/score';
import { CourseDto } from '../../../models/course-dto';
import { lanesIndex } from '../../../pages/courses/constants/lanesIndex';
import { Round } from '../../../models/round';
import { forkJoin, Observable, of } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { CourseService } from '../../../pages/courses/services/course.service';
import { ScoreOrderPipe } from '../../../pipes/score-order.pipe';

@Component({
  selector: 'app-player-table',
  templateUrl: './player-table.component.html',
  styleUrls: ['./player-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayerTableComponent implements OnInit {
  lanesIndex = lanesIndex;

  @Input() score: Score;
  @Input() rounds: Round[];

  @ContentChild('scoreRow', { read: TemplateRef }) scoreRow: TemplateRef<any>;
  @ContentChild('sumRow', { read: TemplateRef }) sumRow: TemplateRef<any>;
  @ContentChild('saveButton', { read: TemplateRef })
  saveButton: TemplateRef<any>;
  @ContentChild('penaltyTable', { read: TemplateRef })
  penaltyTable: TemplateRef<any>;

  courses$: Observable<CourseDto[]>;

  constructor(private courseService: CourseService) {}

  ngOnInit(): void {
    this.score.diffs = ScoreOrderPipe.diffs(this.score.rounds, this.rounds);
    this.courses$ =
      this.rounds.length > 0
        ? forkJoin(
            this.rounds
              .distinctByKey<Round, number>((v) => v.courseId)
              .map((r) => this.courseService.getCached(r.courseId))
          ).pipe(shareReplay())
        : of(null);
  }
}
