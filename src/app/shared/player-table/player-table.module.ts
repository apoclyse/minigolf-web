import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerTableComponent } from './component/player-table.component';
import { SharedPipesModule } from '../../pipes/shared-pipes.module';
import { TableModule } from 'primeng-lts/table';

@NgModule({
  declarations: [PlayerTableComponent],
  exports: [PlayerTableComponent],
  imports: [CommonModule, SharedPipesModule, TableModule],
})
export class PlayerTableModule {}
