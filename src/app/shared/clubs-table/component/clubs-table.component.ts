import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { RoundsService } from '../../../services/rounds.service';
import { ClubsService } from '../../../pages/clubs/services/clubs.service';
import { TeamsService } from '../../../services/teams.service';
import { EventDto } from '../../../models/event-dto';
import { Observable } from 'rxjs';
import { Round } from '../../../models/round';
import { Team } from '../../../models/team';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-clubs-table',
  templateUrl: './clubs-table.component.html',
  styleUrls: ['./clubs-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClubsTableComponent implements OnInit {
  @Input() eventDto: EventDto;

  rounds$: Observable<Round[]>;
  teams$: Observable<Team[]>;

  teamIndexes = [1, 2, 3, 4, 5, 6];

  constructor(
    public roundsService: RoundsService,
    public clubsService: ClubsService,
    public teamsService: TeamsService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.rounds$ = this.roundsService
      .getRounds(this.eventDto.id)
      .pipe(shareReplay());
    this.resetTeams();
  }

  resetTeams(): void {
    this.teams$ = this.teamsService
      .getTeams(this.eventDto.id, this.rounds$)
      .pipe(shareReplay());
    this.changeDetectorRef.detectChanges();
  }
}
