import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { EventDto } from '../../../models/event-dto';
import { ScoresService } from '../../../services/scores.service';
import { ClubsService } from '../../../pages/clubs/services/clubs.service';
import { Score } from '../../../models/score';
import { ResultCategory } from '../../../models/result-category';
import { Round } from '../../../models/round';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { RoundsService } from '../../../services/rounds.service';

@Component({
  selector: 'app-players-table',
  templateUrl: './players-table.component.html',
  styleUrls: ['./players-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayersTableComponent implements OnInit {
  @Input() eventDto: EventDto;
  @Input() resultCategory: ResultCategory = ResultCategory.ALL;

  @Output() selectPlayerEvent: EventEmitter<Score> = new EventEmitter<Score>();

  rounds$: Observable<Round[]>;
  scores$: Observable<Score[]>;

  constructor(
    public scoresService: ScoresService,
    public clubsService: ClubsService,
    private roundsService: RoundsService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.rounds$ = this.roundsService
      .getRounds(this.eventDto.id)
      .pipe(shareReplay());
    this.resetScores();
  }

  resetScores(): void {
    this.scores$ = this.scoresService
      .getScores(this.eventDto.id)
      .pipe(shareReplay());
    this.changeDetectorRef.detectChanges();
  }

  onSelectScore(score: Score): void {
    this.selectPlayerEvent.emit(score);
  }
}
