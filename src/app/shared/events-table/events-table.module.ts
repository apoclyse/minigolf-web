import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsTableComponent } from './component/events-table.component';
import { TableModule } from 'primeng-lts/table';
import { TagModule } from 'primeng-lts/tag';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng-lts/dropdown';
import { SharedPipesModule } from "../../pipes/shared-pipes.module";

@NgModule({
  declarations: [EventsTableComponent],
  exports: [EventsTableComponent],
  imports: [CommonModule, TableModule, TagModule, SharedPipesModule, DropdownModule, FormsModule]
})
export class EventsTableModule {}
