import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { EventsService } from '../../../services/events.service';
import { EventDto } from '../../../models/event-dto';
import { Observable } from 'rxjs';
import { EventState } from "../../../models/event-state";

@Component({
  selector: 'app-events-table',
  templateUrl: './events-table.component.html',
  styleUrls: ['./events-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsTableComponent implements OnInit {
  currentYear: string = new Date().getFullYear().toString();

  @Input() events$: Observable<EventDto[]> = this.eventsService.getAll(this.currentYear);
  @Output() selectEvent: EventEmitter<EventDto> = new EventEmitter<EventDto>();
  dateOptions$: Observable<String[]> = this.eventsService.getYears();

  @ContentChild('itemButtons', { read: TemplateRef })
  itemButtons: TemplateRef<any>;

  eventState = EventState;

  constructor(private eventsService: EventsService) {}

  ngOnInit(): void {}

  onSelectEvent(selectedEvent: EventDto): void {
    this.selectEvent.emit(selectedEvent);
  }

  selectEventYear(year: String): void {
    if (!year) return;
    this.events$ = this.eventsService.getAll(year);
  }
}
