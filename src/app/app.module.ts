import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenubarModule } from 'primeng-lts/menubar';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ConfirmationService, MessageService } from 'primeng-lts/api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastModule } from 'primeng-lts/toast';
import { ButtonModule } from 'primeng-lts/button';
import { PresentationModule } from './pages/presentation/presentation.module';
import { SharedPipesModule } from './pipes/shared-pipes.module';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { ConfirmPopupModule } from 'primeng-lts/confirmpopup';
import './util/array-extension';
import './util/date-extension';
import { DropdownModule } from 'primeng-lts/dropdown';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MenubarModule,
    BrowserAnimationsModule,
    ToastModule,
    ButtonModule,
    PresentationModule,
    SharedPipesModule,
    ConfirmPopupModule,
    DropdownModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    MessageService,
    ConfirmationService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
